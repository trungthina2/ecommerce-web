import React from "react";

// import { ThemeProvider } from "@mui/material";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";

import App from "./App";
import store from "./redux/store";
import reportWebVitals from "./reportWebVitals";
import "./index.scss";
// import theme from "./styles/theme";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

const render = () => {
  root.render(
    <React.StrictMode>
      {/* <ThemeProvider theme={theme}> */}
      <Provider store={store}>
        <App />
      </Provider>
      {/* </ThemeProvider> */}
    </React.StrictMode>
  );
};

render();

// if (process.env.NODE_ENV === "development" && module.hot) {
//   module.hot.accept("./App", render);
// }

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
