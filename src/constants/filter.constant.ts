export const categoryItems = [
  "Apple",
  "ASUS",
  "Dell",
  "Acer",
  "MSI",
  "LG",
  "Huawei",
  "Surface",
  "Lenovo",
  "HP",
  "Gigabyte",
  "Itel",
  "Xiaomi",
  "Masstel",
];

export const CPUItems = [
  "Core I5",
  "Ryzen 5",
  "Ryzen 7",
  "Core I3",
  "Core I7",
  "Apple M2",
  "Apple M1",
  "Celeron",
  "Apple M1 8-core",
  "M2",
  "Core i9",
  "Intel Core I5 Tiger Lake",
  "M1 series",
  "Pentium Silver",
  "Ryzen 3",
];

export const brandItems = [
  "HP",
  "Acer",
  "MSI",
  "Lenovo",
  "Dell",
  "LG",
  "Microsoft",
  "Gigabyte",
  "Itel",
  "Apple",
  "Asus",
  "Xiaomi",
  "Huawei",
];

export const price = [
  { title: "Trên 100 triệu", from: 100000000, to: 0 },
  { title: "2 đến 3 triệu ", from: 2000000, to: 3000000 },
  { title: "3 đến 4 triệu ", from: 3000000, to: 4000000 },
  { title: "4 đến 5 triệu ", from: 4000000, to: 5000000 },
  { title: "5 đến 6 triệu ", from: 5000000, to: 6000000 },
  { title: "6 đến 8 triệu ", from: 6000000, to: 8000000 },
  { title: "8 đến 10 triệu ", from: 8000000, to: 10000000 },
  { title: "10 đến 12 triệu", from: 10000000, to: 12000000 },
  { title: "12 đến 15 triệu", from: 12000000, to: 15000000 },
  { title: "15 đến 20 triệu", from: 15000000, to: 20000000 },
  { title: "20 đến 100 triệu", from: 20000000, to: 100000000 },
];

export const typeItems = ["PC", "Laptop"];

export const resolution = [
  "1920 x 1080",
  "Full HD (1920 x 1080)",
  "1920 x 1200",
  "Retina (2560 x 1600)",
  "2880x1800",
  "2560 x 1600",
  "2560x1600",
  "1366 x 768",
  "1920x1200",
  "2880 x 1800",
  "3456 x 2234",
  "2560x1440",
  "2736 x 1824 pixels",
  "2880 x 1864",
  "3024 x 1964",
  "4.5K (4480x2520)",
  "Liquid Retina (2560 x 1664)",
  "1366x768",
  "2160 x 1440, 185 PPI",
  "2160x1440",
  "2560 x 1440",
  "3456x2160",
  "Aspect ratio: 3:2",
  "FHD (1920x1080)",
  "HD (1366 x 768)",
  "QHD (2160 x 1440)",
  "Resolution: 2736 x 1824 (267 PPI)",
  "Screen: 12.3 inch PixelSense™ Display",
  "Touch: 10 point multi-touch",
  "sRGB 100%",
  "Độ phân giải",
];

export const size = [
  "15.6 inch",
  "14 inch",
  "16 inch",
  "13.3 inch",
  "16.1 inch",
  "11.6 inch",
  "16.2 inch",
  "12.3 inch",
  "13.6 inch",
  "14.2 inch",
  "15.3 inch",
  "17.3 inch",
  "18 inch",
  "24 inch",
  "13.4 inch",
  "17 inch",
];

export const RAM = ["8GB", "16GB", "4GB", "32GB"];

export const GraphicCard = [
  "RTX 3050",
  "GTX 1650",
  "RTX 3050Ti",
  "RTX 3060",
  "RTX 4080",
  "AMD Radeon 660M",
  "MX550",
  "NVIDIA GeForce MX450",
  "NVIDIA GeForce MX550",
  "NVIDIA GeForce RTX 3060",
  "NVIDIA GeForce RTX 4050",
  "NVIDIA GeForce RTX 4060",
  "RX6600M",
];

export const hardDrive = [
  "512GB",

  "256GB",

  "128GB",

  "1TB",

  "2TB",

  "512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 1TB)",

  "Gen4 512GB",

  "Hỗ trợ thêm 1 khe cắm SSD M.2 PCIe mở rộng (nâng cấp tối đa 1TB)",
];

export const sort = [
  { title: "Mặc định", value: 1 },
  { title: "Sản phẩm mới - cũ", value: 2 },
  { title: "Giá thấp đến cao", value: 3 },
  { title: "Giá cao đến thấp", value: 4 },
  { title: "Mới cập nhật", value: 5 },
  { title: "Sản phẩm cũ", value: 6 },
  { title: "Xem nhiều hôm nay", value: 7 },
  { title: "Xem nhiều tuần này", value: 8 },
  { title: "Xem nhiều tháng này", value: 9 },
  { title: "Xem nhiều năm nay", value: 10 },
  { title: "Xem nhiều nhất", value: 11 },
];

export enum sortValue {
  default = 1,
  new_old = 2,
  low_high = 3,
  high_low = 4,
  just_update = 5,
  old = 6,
  most_view_day = 7,
  most_view_week = 8,
  most_view_month = 9,
  most_view_year = 10,
  most_view = 11,
}
