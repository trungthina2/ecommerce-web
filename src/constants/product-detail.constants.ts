/* eslint-disable filenames/match-regex */
export const SALE_TEXT = [
  " Giảm thêm tới 1,5 triệu khi đăng ký gói cước tích điểm MobiFone.",
  " Giảm 1% tối đa 50.000đ cho đơn hàng giá trị từ 3.000.000đ trở lên khi thanh toán qua ZaloPay.",
  "Thanh toán qua Moca tặng ngay đế sạc trị giá 320.000đ (Chohóa đơn có tổng giá trị từ 6 Triệu) - Số lượng có hạn",
  " Giảm thêm tới 800.000đ khi mở thẻ tín dụng TPBank EVO - Duyệt nhanh chỉ 15 phút, Liên hệ cửa hàng hoặc 1900.2091 để được hướng dẫn.",
  " Trả góp qua Homepaylater giảm 10% tối đa 1.000.0000đ (Ápdụng giá trị khoản vay từ 6 triệu)",
  "Mở thẻ tín dụng VPBank nhận ưu đãi tới 1.250.000đ ",
  " Mở thẻ tín dụng VIB - Ưu đãi 250.000đ/thẻ thành công",
  "Hỗ trợ trả góp 0% qua 26 ngân hàng và công ty tài chính.",
];

export const EXTRA_SALE = [
  "Trợ giá lên tới 1.000.000đ khi thu cũ đổi mới lên đời iPhone từ iPhone cũ (trừ mã VN/A) và các sản phẩm khác.",
  "Thu cũ iPhone VN/A giá cao lên đời iPhone mới.",
  "Tặng SIM MobiFone Hera 5G (đơn hàng tại chi nhánh) hoặc SIM Wintel (đơn hàng online / tại chi nhánh) - Áp dụng khi mua Điện thoại, Máy tính bảng, Đồng hồ hoặc các sản phẩm khác với hóa đơn trên 2 Triệu.",
  "Giảm thêm 200.000đ cho tất các sản phẩm màn hình khi mua kèm laptop, MacBook, máy tính bảng và điện thoại",
];

export const FAKE_PRODUCT = [
  {
    id: 1,
    name: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    brand: "Apple",
    price: 2000000,
    sale: "28,890,000 ₫",
    image:
      "https://cdn.hoanghamobile.com/i/productlist/ts/Uploads/2023/02/01/1111.png",
    cpu: "M1",
    ram: "16GB",
    SSD: "256GB",
    card: "GTX-3060 TI",
    OS: "Window 11",
    screen_size: "1920 x 1080",
    resolution: "Full HD",
    weight: " 2.3 KG",
    type: "Cao cấp",
    title: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    content:
      " MacBook Pro 14 M2 Pro/10-core CPU/16-core GPU/16GB/512GB là một trong những sản phẩm mới đáng chú ý của nhà Apple trong năm nay. Với thiết kế tinh tế và thanh lịch, chiếc MacBook này không chỉ là một công cụ làm việc hiệu quả mà còn là một biểu tượng thể hiện sự thành đạt và phong cách của người sử dụng. Bên cạnh đó, thiết bị được sở hữu hiệu năng vô cùng mạnh mẽ và tiên tiến, giúp người dùng dễ dàng làm việc trên nhiều tác vụ đồng thời một cách mượt mà và hiệu quả hơn bao giờ hết",
    availible: true,
  },
  {
    id: 2,
    name: "iPhone 15 Pro Max (256GB) - Chính hãng VN/A",
    brand: "Apple",
    price: 3000000,
    sale: "28,890,000 ₫",
    image:
      "https://cdn.hoanghamobile.com/i/productlist/ts/Uploads/2023/02/01/1111.png",
    cpu: "Ryzen 7",
    ram: "18GB",
    SSD: "256GB",
    card: "GTX-3060 TI",
    OS: "Window 11",
    screen_size: "1920 x 1080",
    resolution: "Full HD",
    weight: " 2.3 KG",
    type: "Cao cấp",
    title: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    content:
      " MacBook Pro 14 M2 Pro/10-core CPU/16-core GPU/16GB/512GB là một trong những sản phẩm mới đáng chú ý của nhà Apple trong năm nay. Với thiết kế tinh tế và thanh lịch, chiếc MacBook này không chỉ là một công cụ làm việc hiệu quả mà còn là một biểu tượng thể hiện sự thành đạt và phong cách của người sử dụng. Bên cạnh đó, thiết bị được sở hữu hiệu năng vô cùng mạnh mẽ và tiên tiến, giúp người dùng dễ dàng làm việc trên nhiều tác vụ đồng thời một cách mượt mà và hiệu quả hơn bao giờ hết",
    availible: true,
  },
  {
    id: 3,
    name: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    brand: "Apple",
    price: 4000000,
    sale: "28,890,000 ₫",
    image:
      "https://cdn.hoanghamobile.com/i/productlist/ts/Uploads/2023/02/01/1111.png",
    cpu: "M1",
    ram: "18GB",
    SSD: "256GB",
    card: "GTX-3060 TI",
    OS: "Window 11",
    screen_size: "1920 x 1080",
    resolution: "Full HD",
    weight: " 2.3 KG",
    type: "Cao cấp",
    title: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    content:
      " MacBook Pro 14 M2 Pro/10-core CPU/16-core GPU/16GB/512GB là một trong những sản phẩm mới đáng chú ý của nhà Apple trong năm nay. Với thiết kế tinh tế và thanh lịch, chiếc MacBook này không chỉ là một công cụ làm việc hiệu quả mà còn là một biểu tượng thể hiện sự thành đạt và phong cách của người sử dụng. Bên cạnh đó, thiết bị được sở hữu hiệu năng vô cùng mạnh mẽ và tiên tiến, giúp người dùng dễ dàng làm việc trên nhiều tác vụ đồng thời một cách mượt mà và hiệu quả hơn bao giờ hết",
    availible: true,
  },
  {
    id: 4,
    name: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    brand: "Apple",
    price: 5000000,
    sale: "28,890,000 ₫",
    image:
      "https://cdn.hoanghamobile.com/i/productlist/ts/Uploads/2023/02/01/1111.png",
    cpu: "M1",
    ram: "18GB",
    SSD: "256GB",
    card: "GTX-3060 TI",
    OS: "Window 11",
    screen_size: "1920 x 1080",
    resolution: "Full HD",
    weight: " 2.3 KG",
    type: "Cao cấp",
    title: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    content:
      " MacBook Pro 14 M2 Pro/10-core CPU/16-core GPU/16GB/512GB là một trong những sản phẩm mới đáng chú ý của nhà Apple trong năm nay. Với thiết kế tinh tế và thanh lịch, chiếc MacBook này không chỉ là một công cụ làm việc hiệu quả mà còn là một biểu tượng thể hiện sự thành đạt và phong cách của người sử dụng. Bên cạnh đó, thiết bị được sở hữu hiệu năng vô cùng mạnh mẽ và tiên tiến, giúp người dùng dễ dàng làm việc trên nhiều tác vụ đồng thời một cách mượt mà và hiệu quả hơn bao giờ hết",
    availible: true,
  },
  {
    id: 5,
    name: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    brand: "Apple",
    price: 6000000,
    sale: "28,890,000 ₫",
    image:
      "https://cdn.hoanghamobile.com/i/productlist/ts/Uploads/2023/02/01/1111.png",
    cpu: "M1",
    ram: "18GB",
    SSD: "256GB",
    card: "GTX-3060 TI",
    OS: "Window 11",
    screen_size: "1920 x 1080",
    resolution: "Full HD",
    weight: " 2.3 KG",
    type: "Cao cấp",
    title: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    content:
      " MacBook Pro 14 M2 Pro/10-core CPU/16-core GPU/16GB/512GB là một trong những sản phẩm mới đáng chú ý của nhà Apple trong năm nay. Với thiết kế tinh tế và thanh lịch, chiếc MacBook này không chỉ là một công cụ làm việc hiệu quả mà còn là một biểu tượng thể hiện sự thành đạt và phong cách của người sử dụng. Bên cạnh đó, thiết bị được sở hữu hiệu năng vô cùng mạnh mẽ và tiên tiến, giúp người dùng dễ dàng làm việc trên nhiều tác vụ đồng thời một cách mượt mà và hiệu quả hơn bao giờ hết",
    availible: true,
  },
  {
    id: 6,
    name: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    brand: "Apple",
    price: 7000000,
    sale: "28,890,000 ₫",
    image:
      "https://cdn.hoanghamobile.com/i/productlist/ts/Uploads/2023/02/01/1111.png",
    cpu: "M1",
    ram: "18GB",
    SSD: "256GB",
    card: "GTX-3060 TI",
    OS: "Window 11",
    screen_size: "1920 x 1080",
    resolution: "Full HD",
    weight: " 2.3 KG",
    type: "Cao cấp",
    title: "iPhone 14 Pro Max (256GB) - Chính hãng VN/A",
    content:
      " MacBook Pro 14 M2 Pro/10-core CPU/16-core GPU/16GB/512GB là một trong những sản phẩm mới đáng chú ý của nhà Apple trong năm nay. Với thiết kế tinh tế và thanh lịch, chiếc MacBook này không chỉ là một công cụ làm việc hiệu quả mà còn là một biểu tượng thể hiện sự thành đạt và phong cách của người sử dụng. Bên cạnh đó, thiết bị được sở hữu hiệu năng vô cùng mạnh mẽ và tiên tiến, giúp người dùng dễ dàng làm việc trên nhiều tác vụ đồng thời một cách mượt mà và hiệu quả hơn bao giờ hết",
    availible: true,
  },
];

export const BLANK_ITEM = {
  id: null,
  name: "",
  brand: "",
  price: "",
  sale: "",
  image: "",
  cpu: "",
  ram: "",
  SSD: "",
  card: "",
  OS: "",
  screen_size: "",
  resolution: "",
  weight: "",
  type: "p",
  title: "",
  content: "",
  availible: true,
};

export enum SHIPPING {
  home = 1,
  store = 2,
}

export const SHIPPING_TYPE: any = {
  [SHIPPING.home]: "Nhận hàng tại nhà",
  [SHIPPING.store]: "Nhận tại cửa hàng",
};
