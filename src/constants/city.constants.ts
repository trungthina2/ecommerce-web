export const CITIES = [
  {
    name: "Hà Nội",
    districts: ["Ba Đình", "Hoàn Kiếm", "Tây Hồ", "Cầu Giấy", "Đống Đa"],
    stores: [
      "194 Lê Duẩn, Hà Nội",
      "382 Nguyễn Văn Cừ, Hà Nội",
      "122 Thái Hà, Hà Nội",
    ],
  },
  {
    name: "Hồ Chí Minh",
    districts: ["Quận 1", "Quận 2", "Quận 3", "Quận 4", "Quận 5"],
    stores: [
      "1060 Đường 3/2, Phường 12, Quận 11, TP HCM",
      "621D Cách Mạng Tháng 8, Phường 15, Quận 10, TP HCM",
      "436 Quang Trung, Gò Vấp, TP.HCM",
    ],
  },
  {
    name: "Đà Nẵng",
    districts: [
      "Hải Châu",
      "Thanh Khê",
      "Sơn Trà",
      "Ngũ Hành Sơn",
      "Liên Chiểu",
    ],
    stores: [
      "153-155 Nguyễn Văn Linh, Đà Nẵng",
      "Số 580 - 582 Điện Biên Phủ, Thanh Khê Đông, Thanh Khê, Đà Nẵng",
      "Số 460 - 462 Đ.Tôn Đức Thắng, P.Hoà Khánh Nam, Q.Liên Chiểu, TP Đà Nẵng",
    ],
  },
  {
    name: "Hải Phòng",
    districts: ["Hồng Bàng", "Ngô Quyền", "Lê Chân", "Kiến An", "Hải An"],
    stores: [
      "72 Lạch Tray, Ngô Quyền, TP Hải Phòng",
      "258 Tô Hiệu - Lê Chân - Hải Phòng",
      "67 Bạch Đằng, TT Núi Đèo, Thủy Nguyên, Hải Phòng",
    ],
  },
  {
    name: "Cần Thơ",
    districts: ["Ninh Kiều", "Bình Thủy", "Cái Răng", "Ô Môn", "Thốt Nốt"],
    stores: [
      "221 Đường 3 Tháng 2 - Ninh Kiều - Cần Thơ",
      "Toà nhà STS, 11B Đại Lộ Hoà Bình, Tân An, Ninh Kiều, Cần Thơ",
    ],
  },
  {
    name: "Đồng Nai",
    districts: [
      "Biên Hòa",
      "Long Khánh",
      "Nhơn Trạch",
      "Trảng Bom",
      "Thống Nhất",
    ],
    stores: [
      "260D Phạm Văn Thuận, TP. Biên Hoà, Đồng Nai",
      "246-256 Lê Duẩn, TT. Long Thành, Đồng Nai",
    ],
  },
  {
    name: "Bình Dương",
    districts: ["Thủ Dầu Một", "Dĩ An", "Bến Cát", "Tân Uyên", "Thuận An"],
    stores: [
      "221 Đường 3 Tháng 2 - Ninh Kiều - Cần Thơ",
      "Toà nhà STS, 11B Đại Lộ Hoà Bình, Tân An, Ninh Kiều, Cần Thơ",
    ],
  },
  {
    name: "Khánh Hòa",
    districts: ["Nha Trang", "Cam Ranh", "Ninh Hòa", "Diên Khánh", "Vạn Ninh"],
    stores: [
      "221 Đường 3 Tháng 2 - Ninh Kiều - Cần Thơ",
      "Toà nhà STS, 11B Đại Lộ Hoà Bình, Tân An, Ninh Kiều, Cần Thơ",
    ],
  },
  {
    name: "Quảng Ninh",
    districts: ["Hạ Long", "Móng Cái", "Uông Bí", "Cẩm Phả", "Mao Khê"],
    stores: [
      "221 Đường 3 Tháng 2 - Ninh Kiều - Cần Thơ",
      "Toà nhà STS, 11B Đại Lộ Hoà Bình, Tân An, Ninh Kiều, Cần Thơ",
    ],
  },
  {
    name: "An Giang",
    districts: ["Long Xuyên", "Châu Đốc", "Tân Châu", "Phú Tân", "Châu Phú"],
    stores: [
      "221 Đường 3 Tháng 2 - Ninh Kiều - Cần Thơ",
      "Toà nhà STS, 11B Đại Lộ Hoà Bình, Tân An, Ninh Kiều, Cần Thơ",
    ],
  },
];
