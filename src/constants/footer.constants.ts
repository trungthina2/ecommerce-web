export const SERVICES_ITEM = [
  "Mua hàng trả góp",
  "Hướng dẫn đặt hàng và thanh toán",
  "Tra cứu đơn hàng",
  "Chính sách bảo hành",
  "Phạm vi, điều khoản gói bảo hành mở rộng",
  "Chính sách bảo mật",
  "Chính sách giải quyết khiếu nại",
  "Điều khoản mua bán hàng hóa",
  "Câu hỏi thường gặp",
];

export const CONTACT_ITEM = [
  "Bán hàng Online",
  "Chăm sóc khách hàng",
  "Dịch vụ sửa chữa Hoàng Hà Care",
  "Liên hệ khối văn phòng",
  "Hợp tác kinh doanh",
  "Tra cứu bảo hành",
];
