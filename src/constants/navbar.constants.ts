export const HEADER_MENU = [
  { label: "Giới thiệu", path: "/introduction" },
  { label: "Sản phẩm đã xem", path: "/viewed" },
  { label: "Trung tâm bảo hành", path: "/service" },
  { label: "Tra cứu đơn hàng", path: "/order" },
];
