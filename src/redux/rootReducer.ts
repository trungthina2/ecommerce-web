import { combineReducers } from "redux";

import { AuthReducer } from "./example.slice";
// combine reducer form all the screen reducer
const rootReducer = combineReducers({
  auth: AuthReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
