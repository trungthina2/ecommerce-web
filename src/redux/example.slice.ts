import { createSlice } from "@reduxjs/toolkit";

type initType = {
  isLogedIn: boolean;
  user: any;
  viewedItems: any[];
};
const initialState: initType = {
  isLogedIn: false,
  user: "",
  viewedItems: [],
};

//  an api === AsyncThunk

// export const getExample = createAsyncThunk(`key name`, ({ data }) => {
//   // return api
//   return exampleApi(data);
// });

const authSlice = createSlice({
  name: "example",
  initialState,
  reducers: {
    setIsLogedIn: state => {
      state.isLogedIn = true;
    },

    setIsNotLogedIn: state => {
      state.isLogedIn = false;
      state.user = "";
      localStorage.clear();
    },

    setUserData: (state, action) => {
      state.user = action.payload;
    },

    setViewedItems: (state, action) => {
      state.viewedItems.push(action.payload);
      sessionStorage.setItem(
        "viewedItems",
        JSON.stringify([...state.viewedItems, action.payload])
      );
    },
  },
  //   extraReducers: builder => {
  //     builder.addCase(getExample.fulfilled, (state, action) => {
  //       state.data = action.payload;
  //     });
  //   },
});

export const { setIsLogedIn, setIsNotLogedIn, setUserData, setViewedItems } =
  authSlice.actions;
export const AuthReducer = authSlice.reducer;
