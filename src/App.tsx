import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Footer from "./components/Footer/Footer";
import Navbar from "./components/NavBar/NavBar";
import { useAppSelector } from "./redux/store";
import CartScreen from "./screens/CartScreen/CartScreen";
import CommentScreen from "./screens/CommentScreen/CommentScreen";
import CompareProduct from "./screens/CompareProductScreen/CompareProductScreen";
import DashboardScreen from "./screens/DashboardScreen/DashboardScreen";
import FavoriteScreen from "./screens/FavoriteScreen/FavoriteScreen";
import ForgotPasswordScreen from "./screens/ForgotPasswordScreen/ForgotPasswordScreen";
import Home from "./screens/Home/Home";
import IntroductionScreen from "./screens/IntroductionScreen/IntroductionScreen";
import LoginScreen from "./screens/LoginScreen/LoginScreen";
import OrderComplete from "./screens/OrderComplete/OrderComplete";
import OrderScreen from "./screens/OrderScreen/OrderScreen";
import ProductDetailScreen from "./screens/ProductDetailScreen/ProductDetailScreen";
import ProductsScreen from "./screens/ProductsScreen/ProductsScreen";
import RateScreen from "./screens/RateScreen/RateScreen";
import RegisterScreen from "./screens/RegisterScreen/RegisterScreen";
import ServiceScreen from "./screens/ServiceScreen/ServiceScreen";
import UserInfoScreen from "./screens/UserInfoScreen/UserInfoScreen";
import ViewedProductScreen from "./screens/ViewedProductScreen/ViewedProductScreen";

function App() {
  const isLogIn = useAppSelector(state => state?.auth.isLogedIn);

  return (
    <>
      <Router>
        {isLogIn ? (
          <>
            <Navbar>
              <Routes>
                <Route path="/user" element={<DashboardScreen />} />
                <Route path="/user/info" element={<UserInfoScreen />} />
                <Route path="/user/order" element={<OrderScreen />} />
                <Route path="/user/favorite" element={<FavoriteScreen />} />
                <Route path="/user/comment" element={<CommentScreen />} />
                <Route path="/user/rate" element={<RateScreen />} />
                <Route path="/" element={<Home />} />
                <Route path="/product/:id" element={<ProductDetailScreen />} />
                <Route path="/compare/" element={<CompareProduct />} />
                <Route path="/introduction" element={<IntroductionScreen />} />
                <Route path="/viewed" element={<ViewedProductScreen />} />
                <Route path="/service" element={<ServiceScreen />} />
                <Route path="/order" element={<OrderComplete />} />
                <Route path="/laptop" element={<ProductsScreen />} />
              </Routes>
            </Navbar>
            <Footer />
          </>
        ) : (
          <>
            <Navbar>
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/product/:id" element={<ProductDetailScreen />} />
                <Route path="/compare/" element={<CompareProduct />} />
                <Route path="/introduction" element={<IntroductionScreen />} />
                <Route path="/viewed" element={<ViewedProductScreen />} />
                <Route path="/service" element={<ServiceScreen />} />
                <Route path="/order" element={<OrderComplete />} />
                <Route path="/laptop" element={<ProductsScreen />} />
                <Route path="/login" element={<LoginScreen />} />
                <Route path="/register" element={<RegisterScreen />} />
                <Route
                  path="/forgot-password"
                  element={<ForgotPasswordScreen />}
                />
                <Route path="/cart" element={<CartScreen />} />
              </Routes>
            </Navbar>
            <Footer />
          </>
        )}
      </Router>
    </>
  );
}

export default App;
