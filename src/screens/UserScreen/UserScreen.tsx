import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import ChatOutlinedIcon from "@mui/icons-material/ChatOutlined";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import LogoutOutlinedIcon from "@mui/icons-material/LogoutOutlined";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import RateReviewOutlinedIcon from "@mui/icons-material/RateReviewOutlined";
import TuneOutlinedIcon from "@mui/icons-material/TuneOutlined";
import UnarchiveOutlinedIcon from "@mui/icons-material/UnarchiveOutlined";
import { Chip, Typography } from "@mui/material";
import cx from "classnames";
import { Link, NavLink, useLocation } from "react-router-dom";

import { setIsNotLogedIn } from "@app/redux/example.slice";
import store from "@app/redux/store";

import styles from "./UserScreen.module.scss";

const UserScreen = ({ children }: any) => {
  const location = useLocation();

  return (
    <div className={cx(styles.root)}>
      <div className={cx(styles.navBar)}>
        <div className="flex-justify-end font-13" style={{ paddingRight: 100 }}>
          <span className="mr-2">
            <Link to="/introduction">Giới thiệu</Link>
          </span>
          <span className="mr-2">
            <Link to="/reports">Sản phẩm đã xem</Link>
          </span>
          <span className="mr-2">
            <Link to="/reports">Trung tâm bảo hành</Link>
          </span>
          <span className="mr-2">
            <Link to="/reports"> Tra cứu đơn hàng</Link>
          </span>
          <span className={cx(styles.userName, "mr-2")}>
            <Link to="/reports">username</Link>
            <div className={styles.menuDropDown}>
              <ul>
                <li>
                  <NavLink className="flex-align-center" to="/user">
                    <TuneOutlinedIcon className="font-15 mr-6" />
                    <Typography className="font-12">Bảng điều khiển</Typography>
                  </NavLink>
                </li>
                <li>
                  <NavLink className="flex-align-center" to="/user/info">
                    <AccountCircleOutlinedIcon className="font-15 mr-6" />
                    <Typography className="font-12">
                      Thông tin tài khoản
                    </Typography>
                  </NavLink>
                </li>
                <li>
                  <NavLink className="flex-align-center" to="/user/order">
                    <UnarchiveOutlinedIcon className="font-15 mr-6" />
                    <Typography className="font-12">
                      Đơn hàng của bạn
                    </Typography>
                  </NavLink>
                </li>
                <li>
                  <NavLink className="flex-align-center" to="/user/favorite">
                    <FavoriteBorderOutlinedIcon className="font-15 mr-6" />
                    <Typography className="font-12">
                      Sản phẩm yêu thích
                    </Typography>
                  </NavLink>
                </li>
                <li>
                  <NavLink className="flex-align-center" to="/user/comment">
                    <ChatOutlinedIcon className="font-15 mr-6" />
                    <Typography className="font-12">
                      Quản lí bình luận
                    </Typography>
                  </NavLink>
                </li>
                <li>
                  <NavLink className="flex-align-center" to="/user/rate">
                    <RateReviewOutlinedIcon className="font-15 mr-6" />
                    <Typography className="font-12">
                      Quản lí đánh giá
                    </Typography>
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className="flex-align-center"
                    to=""
                    onClick={() => console.log("log out")}
                  >
                    <LogoutOutlinedIcon className="font-15 mr-6" />
                    <Typography className="font-12">Đăng xuất</Typography>
                  </NavLink>
                </li>
              </ul>
            </div>
          </span>
        </div>
      </div>
      <div style={{ width: "100%" }} className="d-flex flex-space around">
        <div className={styles.sideBar}>
          <div className="pl-5 py-8">
            <div>
              <Link to="/">
                <img
                  src="https://hoanghamobile.com/Content/web/img/logo-text.png"
                  alt="logo"
                  width="90%"
                />
              </Link>
            </div>

            <div className="d-flex flex-align-center mt-5 ">
              <Chip
                className="font-40 font-weight-bold mt-1 mr-3"
                label="U"
                style={{
                  backgroundColor: "#009981",
                  color: "#fff",
                  borderRadius: 20,
                  height: 60,
                  width: 60,
                }}
              />
              <span>
                <Typography className="font-16 font-weight-bold">
                  User name
                </Typography>
                <div
                  className="d-flex flex-align-center"
                  style={{ color: "#999" }}
                >
                  <PersonOutlineOutlinedIcon className="font-13 " />
                  <Typography className="font-13">
                    Thay đổi ảnh đại diện
                  </Typography>
                </div>
              </span>
            </div>

            {/* side navigate */}
            <ul className="mt-9">
              <li
                className={
                  location.pathname === "/user"
                    ? cx(styles.active, "mb-7")
                    : "mb-7"
                }
              >
                <NavLink className="flex-align-center" to="/user">
                  <TuneOutlinedIcon className="font-25 mr-6" />
                  <Typography className="font-weight-bold">
                    Bảng điều khiển
                  </Typography>
                </NavLink>
              </li>
              <li
                className={
                  location.pathname === "/user/info"
                    ? cx(styles.active, "mb-7")
                    : "mb-7"
                }
              >
                <NavLink className="flex-align-center" to="/user/info">
                  <AccountCircleOutlinedIcon className="font-25 mr-6" />
                  <Typography className="font-weight-bold">
                    Thông tin tài khoản
                  </Typography>
                </NavLink>
              </li>
              <li
                className={
                  location.pathname === "/user/order"
                    ? cx(styles.active, "mb-7")
                    : "mb-7"
                }
              >
                <NavLink className="flex-align-center" to="/user/order">
                  <UnarchiveOutlinedIcon className="font-25 mr-6" />
                  <Typography className="font-weight-bold">
                    Đơn hàng của bạn
                  </Typography>
                </NavLink>
              </li>
              <li
                className={
                  location.pathname === "/user/favorite"
                    ? cx(styles.active, "mb-7")
                    : "mb-7"
                }
              >
                <NavLink className="flex-align-center" to="/user/favorite">
                  <FavoriteBorderOutlinedIcon className="font-25 mr-6" />
                  <Typography className="font-weight-bold">
                    Sản phẩm yêu thích
                  </Typography>
                </NavLink>
              </li>
              <li
                className={
                  location.pathname === "/user/comment"
                    ? cx(styles.active, "mb-7")
                    : "mb-7"
                }
              >
                <NavLink className="flex-align-center" to="/user/comment">
                  <ChatOutlinedIcon className="font-25 mr-6" />
                  <Typography className="font-weight-bold">
                    Quản lí bình luận
                  </Typography>
                </NavLink>
              </li>
              <li
                className={
                  location.pathname === "/user/rate"
                    ? cx(styles.active, "mb-7")
                    : "mb-7"
                }
              >
                <NavLink className="flex-align-center" to="/user/rate">
                  <RateReviewOutlinedIcon className="font-25 mr-6" />
                  <Typography className="font-weight-bold">
                    Quản lí đánh giá
                  </Typography>
                </NavLink>
              </li>
              <li className="mb-7">
                <NavLink
                  className="flex-align-center"
                  to=""
                  onClick={() => store.dispatch(setIsNotLogedIn())}
                >
                  <LogoutOutlinedIcon className="font-25 mr-6" />
                  <Typography className="font-weight-bold">
                    Đăng xuất
                  </Typography>
                </NavLink>
              </li>
            </ul>
          </div>
        </div>

        {/* main body */}
        <div className={cx(styles.mainBody)}>
          <div className="mt-6">{children}</div>
        </div>
      </div>
    </div>
  );
};

export default UserScreen;
