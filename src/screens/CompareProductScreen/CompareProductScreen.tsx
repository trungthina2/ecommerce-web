import React, { useEffect, useState } from "react";

import RemoveCircleIcon from "@mui/icons-material/RemoveCircle";
import {
  TableCell,
  Container,
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableBody,
  Paper,
  Typography,
  Button,
  Grid,
} from "@mui/material";
import cx from "classnames";

import {
  FAKE_PRODUCT,
  SALE_TEXT,
} from "@app/constants/product-detail.constants";

import styles from "./CompareProductScreen.module.scss";

const CompareProductScreen = () => {
  const item1Id = JSON.parse(sessionStorage.getItem("item1Id") ?? "");
  const item2Id = JSON.parse(sessionStorage.getItem("item2Id") ?? "");
  const isDeleteItem2 = JSON.parse(
    sessionStorage.getItem("isDeleteItem2Id") ?? ""
  );

  // get item to compare by id
  const [item1, setItem1] = useState<any>(FAKE_PRODUCT[Number(item1Id) - 1]);
  const [item2, setItem2] = useState<any>(
    isDeleteItem2 ? undefined : FAKE_PRODUCT[Number(item2Id) - 1]
  );
  const [searchData, setSearchData] = useState<any[]>([]);

  const createData = (name: string, firstItem: any, secondItem: any) => {
    return { name, firstItem, secondItem };
  };

  useEffect(() => {
    if (item2 === undefined) {
      sessionStorage.setItem("isDeleteItem2Id", "true");
    }
  }, [item2, item2Id]);

  const handleSearch = (value: string) => {
    if (value === "") {
      setSearchData([]);
      return;
    }

    setSearchData(
      FAKE_PRODUCT.filter((item: any) => {
        const lowerCaseItem = item.name.toLowerCase().trim();
        const lowerCaseSearchText = value.toLowerCase().trim();

        return lowerCaseItem.includes(lowerCaseSearchText);
      })
    );
  };

  const commonDataRows = [
    createData(
      "Hình ảnh, giá",
      <span
        className="m-0 flex-center flex-direction-column"
        style={{ maxWidth: 480, position: "relative" }}
      >
        <Button
          className={!item2 ? "d-none" : ""}
          style={{ position: "absolute", right: 0, top: 0 }}
          onClick={() => {
            setItem1(item2);
            setItem2(undefined);
          }}
        >
          <RemoveCircleIcon style={{ color: "red" }} />
        </Button>
        <img src={item1?.image} alt="test" />
        <Typography className="font-weight-bold">{item1?.name}</Typography>
        <Typography className="font-25" style={{ color: "red" }}>
          {item1?.price}
        </Typography>
        <Typography className="font-15">| Giá đã bao gồm 10% VAT</Typography>
      </span>,
      <span>
        {item2 ? (
          <span
            className="m-0 flex-center flex-direction-column"
            style={{ maxWidth: 480, position: "relative" }}
          >
            <Button
              style={{ position: "absolute", right: 0, top: 0 }}
              onClick={() => {
                setItem2(undefined);
              }}
            >
              <RemoveCircleIcon style={{ color: "red" }} />
            </Button>
            <img src={item2?.image} alt="test" />
            <Typography className="font-weight-bold">{item2?.name}</Typography>
            <Typography className="font-25" style={{ color: "red" }}>
              {item2?.price}
            </Typography>
            <Typography className="font-15">
              | Giá đã bao gồm 10% VAT
            </Typography>
          </span>
        ) : (
          <span
            className="m-0 flex-center flex-direction-column"
            style={{ maxWidth: 480, position: "relative" }}
          >
            <Typography className="mb-8">
              Bạn muốn so sánh thêm sản phẩm?
            </Typography>
            <input
              className={cx(styles.searchBox, "font-14")}
              placeholder="Tìm kiếm sản phẩm"
              // {...register("searchkey")}
              onChange={e => handleSearch(e.target.value.trim())}
            />
            {searchData.length > 0 && (
              <div className={cx(styles.searchResult)}>
                <ul>
                  {searchData.map((item: any) => (
                    <button
                      onClick={() => {
                        setItem2(item);
                        setSearchData([]);
                        sessionStorage.setItem("isDeleteItem2Id", "false");
                      }}
                    >
                      <li>
                        <Grid container style={{ maxHeight: "70px" }}>
                          <Grid item xs={2}>
                            <img src={item.imgage} alt="tét" width="70%" />
                          </Grid>
                          <Grid
                            item
                            xs={10}
                            className="flex-direction-column flex-space-between d-flex"
                          >
                            <Grid item xs={12}>
                              <Typography
                                className="font-weight-bold"
                                style={{ color: "#000" }}
                              >
                                {item.name}
                              </Typography>
                            </Grid>
                            <Grid item xs={12}>
                              <Typography
                                className="font-15 font-weight-bold"
                                style={{ color: "#fd475a" }}
                              >
                                {item?.price}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Grid>
                      </li>
                    </button>
                  ))}
                </ul>
              </div>
            )}
          </span>
        )}
      </span>
    ),
    createData(
      "Khuyến mại",
      <ul>
        {SALE_TEXT.map(item => (
          <li>{item}</li>
        ))}
      </ul>,
      ""
    ),
    createData(
      "Bộ sản phẩm tiêu chuẩn",
      item1?.combo ?? "",
      item2?.combo ?? ""
    ),
    createData("Bảo hành", item1?.warranty ?? "", item2?.warranty ?? ""),
  ];

  const hardwareDataRows = [
    createData("Hãng CPU", item1?.cpu, item2?.cpu),
    createData("Dòng CPU", item1?.cpu, item2?.cpu),
    createData("Số hiệu CPU", "", ""),
    createData("Số nhân	", "", ""),
    createData("Số luồng", "", ""),
    createData("Xung nhịp cơ bản", "2.70 Ghz", ""),
    createData("Xung nhịp tối đa", "4.10 Ghz", ""),
    createData("Bộ nhớ đệm", "8 MB", ""),
  ];

  const soundDataRows = [
    createData("Card on-board", "", ""),
    createData("Card đồ hoạ rời", item1?.card, item2?.card),
    createData("VRAM card đồ hoạ rời", "4MB", ""),
    createData("Công nghệ âm thanh", "", ""),
  ];

  const memoryDataRows = [
    createData("RAM", item1?.ram, item2?.ram),
    createData("Tiêu chuẩn RAM", "", ""),
    createData("Tốc độ Bus", "", ""),
    createData("Tổng số khe RAM có thể thay thế/lắp thêm", "2", "2"),
    createData("Số khe RAM trống", "1", "1"),
    createData("Dạng chân cắm", "", ""),
    createData("Hỗ trợ RAM tối đa", "32GB", ""),
    createData("Ổ cứng mặc định", item1?.SSD, item2?.SSD),
    createData("Loại ổ cứng", "NVME", ""),
    createData("Khả năng nâng cấp", "có", ""),
  ];

  const screenDataRows = [
    createData("Kích thước màn hình", item1?.screen_size, item2?.screen_size),
    createData("Độ phân giải", item1?.resolution, item2?.resolution),
    createData("Loại tấm nền", "IPS", ""),
    createData("Hỗ trợ cảm ứng", "Không", "Không"),
    createData("Tần số quét", "120Hz", "120Hz"),
    createData("Công nghệ màn hình", "", ""),
  ];

  const keyboardDataRows = [
    createData("Cấu trúc bàn phím	", "", ""),
    createData("Đèn nền bàn phím", "", ""),
    createData("Touchpad", "", ""),
  ];

  const portDataRows = [
    createData("Các cổng giao tiếp", "", ""),
    createData("Kết nối không dây", "", ""),
  ];

  const softwareDataRows = [
    createData("Hệ điều hành", item1?.OS, item2?.OS),
    createData("Phần mềm sẵn có", "", ""),
  ];

  const sizeDataRows = [
    createData("Kích thước", "", ""),
    createData("Trọng lượng", item1?.weight, item2?.weight),
  ];

  const batteryDataRows = [
    createData("Dung lượng pin", "", ""),
    createData("Bộ sạc theo máy", "có", ""),
  ];

  const otherDataRows = [
    createData("Thời điểm ra mắt", "", ""),
    createData("Chất liệu", "", ""),
  ];

  return (
    <div>
      <Container className={cx(styles.root, "my-9 p-6")} maxWidth="lg">
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  THÔNG TIN CHUNG
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {commonDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 150, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
            {/* hardware infomation */}
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  BỘ VI XỬ LÝ
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {hardwareDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* sound data */}
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  ĐỒ HỌA VÀ ÂM THANH
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {soundDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* memory data */}
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  BỘ NHỚ RAM, Ổ CỨNG
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {memoryDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* màn hình */}

            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  MÀN HÌNH
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {screenDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* keyboard data */}

            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  BÀN PHÍM & TOUCHPAD
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {keyboardDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* port data */}
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  CỔNG KẾT NỐI & TÍNH NĂNG MỞ RỘNG
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {portDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* software data */}
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  PHẦN MỀM
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {softwareDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* size data */}
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  KÍCH THƯỚC & TRỌNG LƯỢNG
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {sizeDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* battery data */}
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  PIN VÀ SẠC
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {batteryDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>

            {/* ohter data */}
            <TableHead>
              <TableRow>
                <TableCell
                  className={cx(
                    styles.tableHead,
                    "font-weight-bold font-16 text-center"
                  )}
                  colSpan={12}
                >
                  THÔNG TIN KHÁC
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {otherDataRows.map(row => (
                <TableRow key={row.name}>
                  <TableCell
                    style={{ width: 100, backgroundColor: "#EFEFEF" }}
                    component="th"
                    className="text-center font-weight-bold"
                    colSpan={4}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.firstItem}
                  </TableCell>
                  <TableCell style={{ maxWidth: 480 }} align="left" colSpan={5}>
                    {row.secondItem}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
    </div>
  );
};

export default CompareProductScreen;
