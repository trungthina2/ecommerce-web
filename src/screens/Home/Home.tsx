import { Container, Grid } from "@mui/material";
import cx from "classnames";
import { useNavigate } from "react-router-dom";
import Slider from "react-slick";

import BannerListComponent from "@app/components/BannerListComponent/BannerListComponent";
import SliderComponent from "@app/components/Slider/Sliders";
import { setViewedItems } from "@app/redux/example.slice";
import store from "@app/redux/store";

import { APPLE_PRODUCTS, FAVORITE_PRODUCTS, QUICK_SALES } from "./Constants";
import styles from "./Home.module.scss";

const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        initialSlide: 2,
      },
    },
  ],
};
const Home = () => {
  const navigate = useNavigate();
  return (
    <Container maxWidth="lg" className={cx(styles.container)}>
      <Grid container>
        <Grid item xs={12}>
          <SliderComponent />
        </Grid>
      </Grid>
      <Grid
        container
        className={cx(styles.quick_sales)}
        spacing={1}
        columns={12}
      >
        {QUICK_SALES.map(item => {
          return (
            <Grid item sm={6} md={3} lg={3} key={item.id}>
              <a href="/">
                <img
                  src={`/img/${item.img}`}
                  alt=""
                  className={cx(styles.img_quick_sale)}
                />
              </a>
            </Grid>
          );
        })}
      </Grid>
      <Grid container className={cx(styles.fp_container)}>
        <BannerListComponent
          data={{
            text: "CÓ THỂ BẠN SẼ THÍCH",
          }}
        />
        <Grid item xs={12}>
          <Slider {...settings}>
            {FAVORITE_PRODUCTS.map(item => {
              return (
                <button
                  onClick={() => {
                    store.dispatch(setViewedItems(item));
                    navigate("/product/:id".replace(":id", item.id.toString()));
                  }}
                >
                  <div
                    className={cx(styles.fp_card_container, "cursor-pointer")}
                    key={item.id}
                  >
                    <div className={cx(styles.fp_card_header)}>
                      <div>
                        <div
                          className={cx(styles.fp_card_img)}
                          data-image={`/img/${item.img}`}
                          style={{ backgroundImage: `url(/img/${item.img})` }}
                        />
                      </div>
                    </div>
                    <div>
                      <div className={cx(styles.fp_card_name)}>{item.name}</div>
                      <div className={cx(styles.fp_card_price)}>
                        {item.price}
                        <span>{item.sale && ` ${item.sale}`}</span>
                      </div>
                    </div>
                  </div>
                </button>
              );
            })}
          </Slider>
        </Grid>
      </Grid>
      <BannerListComponent
        data={{
          backgroundColor: "#009981",
          colorMain: "#00483d",
          text: "ĐIỆN THOẠI NỔI BẬT",
          color: "white",
        }}
      />
      <Grid
        container
        className={cx(styles.list_product_container)}
        spacing={1}
        columns={30}
      >
        {APPLE_PRODUCTS.map(item => {
          return (
            <Grid item xs={15} md={10} lg={6} key={item.id}>
              <button
                onClick={() => {
                  store.dispatch(setViewedItems(item));
                  navigate("/product/:id".replace(":id", item.id.toString()));
                }}
              >
                <div className={cx(styles.item_product_container)}>
                  <div className={cx(styles.item_product_header)}>
                    <img
                      src={`/img/${item.image}`}
                      alt=""
                      className={cx(styles.img_product)}
                    />
                    <div className={cx(styles.promote)}>
                      <ul>
                        <li>
                          <span className={cx(styles.bag)}>KM</span> Giảm thêm
                          tới 1,5 triệu khi đăng ký gói cước tích điểm MobiFone.
                        </li>
                        <li>
                          <span className={cx(styles.bag)}>KM</span> Giảm tới
                          150.000đ khi thanh toán qua VNPAY-QR (Áp dụng cho đơn
                          hàng trên 3.000.000đ).
                        </li>
                        <li>
                          <span className={cx(styles.bag)}>KM</span> Giảm thêm
                          tới 1.000.000đ khi Thu cũ - Lên đời iPhone 11 | 12 |
                          13 Series
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className={cx(styles.item_product_content)}>
                    <div className={cx(styles.title)}>{item.name}</div>
                    <div className={cx(styles.price)}>
                      <strong>{item.price}</strong>
                      <small>{item.cost}</small>
                    </div>
                    <div className={cx(styles.up_form)}>
                      <p>
                        Giá lên đời từ:{" "}
                        <strong className={cx(styles.text_red)}>
                          {item.subsidy}
                        </strong>
                      </p>
                    </div>
                  </div>
                  <div className={cx(styles.note)}>
                    <span className={cx(styles.bag)}>KM</span>{" "}
                    <span title="Giảm thêm tới 1,5 triệu khi đăng ký gói cước tích điểm MobiFone.">
                      Giảm thêm tới 1,5 triệu khi đăng ký...
                    </span>
                    <strong className={cx(styles.text_orange)}>
                      VÀ 10 KM KHÁC
                    </strong>
                  </div>
                </div>
              </button>
            </Grid>
          );
        })}
      </Grid>
    </Container>
  );
};

export default Home;
