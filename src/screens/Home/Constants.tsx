import LaptopIcon from "@mui/icons-material/Laptop";

export const MENUS = [
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
  {
    name: "LAPTOP",
    icon: <LaptopIcon />,
  },
];

export const QUICK_SALES = [
  {
    id: 1,
    img: "quick_sale1.png",
  },
  {
    id: 2,
    img: "quick_sale2.png",
  },
  {
    id: 3,
    img: "quick_sale3.png",
  },
  {
    id: 4,
    img: "quick_sale4.png",
  },
];

export const FAVORITE_PRODUCTS = [
  {
    id: 1,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "156.490.000 ₫",
    img: "fp_ip13_red.jpeg",
  },
  {
    id: 2,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "",
    img: "fp_ip13_red.jpeg",
  },
  {
    id: 3,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "",
    img: "fp_ip13_red.jpeg",
  },
  {
    id: 4,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    img: "fp_ip13_red.jpeg",
    sale: "",
  },
  {
    id: 5,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "",
    img: "fp_ip13_red.jpeg",
  },
  {
    id: 6,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "",
    img: "fp_ip13_red.jpeg",
  },
  {
    id: 7,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "",
    img: "fp_ip13_red.jpeg",
  },
  {
    id: 8,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "",
    img: "fp_ip13_red.jpeg",
  },
  {
    id: 9,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "",
    img: "fp_ip13_red.jpeg",
  },
  {
    id: 10,
    name: "iPhone 13 (128GB) - Chính hãng VN/A",
    price: "15.990.000 ₫",
    sale: "",
    img: "fp_ip13_red.jpeg",
  },
];

export const APPLE_PRODUCTS = [
  {
    id: 1,
    name: "iPhone 11 (64GB) - Chính hãng VN/A",
    price: "10,390,000 ₫",
    cost: "19,990,000 ₫",
    subsidy: "9,890,000 ₫",
    image: "image-product-1.webp",
  },
  {
    id: 2,
    name: "iPhone 11 (64GB) - Chính hãng VN/A",
    price: "10,390,000 ₫",
    cost: "19,990,000 ₫",
    subsidy: "9,890,000 ₫",
    image: "image-product-1.webp",
  },
  {
    id: 3,
    name: "iPhone 11 (64GB) - Chính hãng VN/A",
    price: "10,390,000 ₫",
    cost: "19,990,000 ₫",
    subsidy: "9,890,000 ₫",
    image: "image-product-1.webp",
  },
  {
    id: 4,
    name: "iPhone 11 (64GB) - Chính hãng VN/A",
    price: "10,390,000 ₫",
    cost: "19,990,000 ₫",
    subsidy: "9,890,000 ₫",
    image: "image-product-1.webp",
  },
  {
    id: 5,
    name: "iPhone 11 (64GB) - Chính hãng VN/A",
    price: "10,390,000 ₫",
    cost: "19,990,000 ₫",
    subsidy: "9,890,000 ₫",
    image: "image-product-1.webp",
  },
  {
    id: 6,
    name: "iPhone 11 (64GB) - Chính hãng VN/A",
    price: "10,390,000 ₫",
    cost: "19,990,000 ₫",
    subsidy: "9,890,000 ₫",
    image: "image-product-1.webp",
  },
  {
    id: 7,
    name: "iPhone 11 (64GB) - Chính hãng VN/A",
    price: "10,390,000 ₫",
    cost: "19,990,000 ₫",
    subsidy: "9,890,000 ₫",
    image: "image-product-1.webp",
  },
  {
    id: 8,
    name: "iPhone 11 (64GB) - Chính hãng VN/A",
    price: "10,390,000 ₫",
    cost: "19,990,000 ₫",
    subsidy: "9,890,000 ₫",
    image: "image-product-1.webp",
  },
];
