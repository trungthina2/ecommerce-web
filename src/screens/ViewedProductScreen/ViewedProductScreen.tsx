import React, { useEffect, useState } from "react";

import { Container, Grid, Typography } from "@mui/material";
import cx from "classnames";
import { uniqBy } from "lodash";

import { formattedNumber } from "@app/helpers/ultils";

import styles from "./ViewedProductScreen.module.scss";

const ViewedProductScreen = () => {
  const viewed = sessionStorage.getItem("viewedItems");

  const [datas, setDatas] = useState<any>([]);

  useEffect(() => {
    if (viewed !== null) {
      setDatas(uniqBy(JSON.parse(viewed), "id"));
    }
  }, [viewed]);

  return (
    <div>
      <Container className="mb-5" maxWidth="lg">
        <Typography className="font-22 font-weight-bold">
          Sản phẩm đã xem
        </Typography>
        <Grid
          container
          className={cx(styles.list_product_container, "mt-4")}
          spacing={1}
          columns={10}
        >
          {datas.map((item: any) => {
            return (
              <Grid item xs={2} key={item.id}>
                <div className={cx(styles.item_product_container)}>
                  <div className={cx(styles.item_product_header)}>
                    <img
                      src={`/img/${item.image}`}
                      alt=""
                      className={cx(styles.img_product)}
                    />
                    <div className={cx(styles.promote)}>
                      <a
                        href={"/product/:id".replace(":id", item.id.toString())}
                      >
                        <ul>
                          <li>
                            <span className={cx(styles.bag)}>KM</span> Giảm thêm
                            tới 1,5 triệu khi đăng ký gói cước tích điểm
                            MobiFone.
                          </li>
                          <li>
                            <span className={cx(styles.bag)}>KM</span> Giảm tới
                            150.000đ khi thanh toán qua VNPAY-QR (Áp dụng cho
                            đơn hàng trên 3.000.000đ).
                          </li>
                          <li>
                            <span className={cx(styles.bag)}>KM</span> Giảm thêm
                            tới 1.000.000đ khi Thu cũ - Lên đời iPhone 11 | 12 |
                            13 Series
                          </li>
                        </ul>
                      </a>
                    </div>
                  </div>
                  <div className={cx(styles.item_product_content)}>
                    <a
                      href={"/product/:id".replace(":id", item.id.toString())}
                      className={cx(styles.title)}
                    >
                      {item.name}
                    </a>
                    <div className={cx(styles.price)}>
                      <strong>{formattedNumber(item?.price ?? 0)}</strong>
                      <small>{formattedNumber(item?.cost ?? 0)}</small>
                    </div>
                    <div className={cx(styles.up_form)}>
                      <p>
                        Giá lên đời từ:{" "}
                        <strong className={cx(styles.text_red)}>
                          {item.subsidy}
                        </strong>
                      </p>
                    </div>
                  </div>
                  <div className={cx(styles.note)}>
                    <span className={cx(styles.bag)}>KM</span>{" "}
                    <span title="Giảm thêm tới 1,5 triệu khi đăng ký gói cước tích điểm MobiFone.">
                      Giảm thêm tới 1,5 triệu khi đăng ký...
                    </span>
                    <strong className={cx(styles.text_orange)}>
                      VÀ 10 KM KHÁC
                    </strong>
                  </div>
                </div>
              </Grid>
            );
          })}
        </Grid>
      </Container>
    </div>
  );
};

export default ViewedProductScreen;
