import React from "react";

import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import TaskAltIcon from "@mui/icons-material/TaskAlt";
import {
  Container,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import cx from "classnames";
import { useNavigate, Link, useLocation } from "react-router-dom";

import { SHIPPING_TYPE } from "@app/constants/product-detail.constants";
import { formattedNumber } from "@app/helpers/ultils";

import styles from "./OrderComplete.module.scss";

const OrderComplete = () => {
  const navigate = useNavigate();

  const { state } = useLocation();

  const createData = (
    order: string,
    name: string,
    version: string,
    color: string,
    quantity: number,
    price: string,
    sum: string
  ) => {
    return {
      order,
      name,
      version,
      color,
      quantity,
      price,
      sum,
    };
  };

  const createOrderData = (
    order: string,
    status: string,
    sender: string,
    note: string,
    time: string
  ) => {
    return {
      order,
      status,
      sender,
      note,
      time,
    };
  };

  const Total = state?.productData
    ? state?.userRecieveInfomation?.quantity * state?.productData.price
    : state?.listCart?.reduce((acc: any, item: any) => {
        return acc + item.quantity * item.price;
      }, 0);

  const orders = state?.productData
    ? [
        createData(
          "",
          state?.productData.title,
          state?.productData.type,
          state?.productData.color,
          state?.userRecieveInfomation?.quantity,
          formattedNumber(state?.productData.price),
          formattedNumber(
            state?.userRecieveInfomation?.quantity * state?.productData.price
          )
        ),
      ]
    : state?.listCart?.map((item: any) =>
        createData(
          "",
          item.title,
          item.type,
          item.color,
          item.quantity,
          formattedNumber(item.price),
          formattedNumber(item.quantity * item.price)
        )
      );

  // list of history order data
  const listOfOrder = [
    createOrderData(
      "",
      "Đã nhận hàng",
      "",
      "Thanh toán khi nhận hàng",
      "10/1/2023"
    ),
  ];
  return (
    <Container maxWidth="lg" className="mb-5">
      <div className={cx(styles.root)}>
        <button
          className="d-flex flex-align-center cursor-pointer"
          onClick={() => navigate("/")}
        >
          <ArrowBackIosIcon className="font-40 font-weight-normal" />
          <Typography>Quay lại</Typography>
        </button>
        <Grid container>
          <Grid item xs={12} className="flex-center flex-direction-column">
            <TaskAltIcon className={cx(styles.checkIcon)} />
            <div className="d-flex my-3">
              <Typography className=" font-weight-bold">
                THÔNG TIN ĐƠN HÀNG SỐ
              </Typography>
              <Typography
                className=" font-weight-bold"
                style={{ color: "#ff5801" }}
              >
                12341234SSSASM
              </Typography>
            </div>
          </Grid>
        </Grid>
        <div className={cx(styles.orderInfoWrapper)}>
          <Typography className="mb-4 font-weight-bold">
            1.Thông tin người đặt hàng
          </Typography>
          <Grid container spacing={4} className="d-flex font-14">
            <Grid item xs={5}>
              <Grid item xs={12} className="d-flex">
                <Grid item xs={5}>
                  Họ tên:
                </Grid>
                <Grid item xs={7}>
                  {state?.userRecieveInfomation.fullname ??
                    state?.userRecieveInfomation.username}
                </Grid>
              </Grid>

              <Grid item xs={12} className="d-flex">
                <Grid item xs={5}>
                  Phương thức:
                </Grid>
                <Grid item xs={7}>
                  {
                    SHIPPING_TYPE[
                      state?.userRecieveInfomation.deliveryForm ??
                        state?.userRecieveInfomation.type
                    ]
                  }
                </Grid>
              </Grid>

              <Grid item xs={12} className="d-flex">
                <Grid item xs={5}>
                  Nhận hàng tại :
                </Grid>
                <Grid item xs={7}>
                  {state?.userRecieveInfomation.address}
                </Grid>
              </Grid>

              <Grid item xs={12} className="d-flex">
                <Grid item xs={5}>
                  Ghi chú:
                </Grid>
                <Grid item xs={7}>
                  {state?.userRecieveInfomation.note}
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={5}>
              <Grid item xs={12} className="d-flex">
                <Grid item xs={5}>
                  Điện thoại:
                </Grid>
                <Grid item xs={7}>
                  {state?.userRecieveInfomation.phone ??
                    state?.userRecieveInfomation.phonenumber}
                </Grid>
              </Grid>
              <Grid item xs={12} className="d-flex">
                <Grid item xs={5}>
                  Email:
                </Grid>
                <Grid item xs={7}>
                  {state?.userRecieveInfomation.email}
                </Grid>
              </Grid>

              <Grid item xs={12} className="d-flex">
                <Grid item xs={5}>
                  Trạng thái:
                </Grid>
                <Grid item xs={7} style={{ color: "red" }}>
                  Đã xác nhận thông tin đặt hàng
                </Grid>
              </Grid>

              <Grid item xs={12} className="d-flex">
                <Grid item xs={5}>
                  Chỉ đường:
                </Grid>
                <Grid item xs={7}>
                  <Link to="/" style={{ color: "#009981" }}>
                    Xem tại đây
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>

        <div className={cx(styles.orderInfoWrapper, "my-4")}>
          <Typography className="mb-4 font-weight-bold">
            2.Danh sách sản phẩm đặt hàng
          </Typography>
          <div>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className={cx(styles.tableHead)}>#</TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Tên sản phẩm
                    </TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Phiên bản
                    </TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Màu sắc
                    </TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      SL
                    </TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Giá tiền
                    </TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Tổng (SL x G )
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody style={{ border: "1px solid #ccc" }}>
                  {orders?.map((row: any) => (
                    <TableRow
                      key={row.name}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {row.order}
                      </TableCell>
                      <TableCell align="center">{row.name}</TableCell>
                      <TableCell align="center">{row.version}</TableCell>
                      <TableCell align="center">{row.color}</TableCell>
                      <TableCell align="center">{row.quantity}</TableCell>
                      <TableCell align="center">{row.price}</TableCell>
                      <TableCell align="center">{row.sum}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
                <caption className="text-right">
                  <Grid container className="d-flex font-14 flex-justify-end">
                    <Grid item xs={6}>
                      <Grid item xs={12} className="d-flex">
                        <Grid item xs={5}>
                          Tổng tiền:
                        </Grid>
                        <Grid item xs={7}>
                          {formattedNumber(Total ?? 0)}
                        </Grid>
                      </Grid>
                      <Grid item xs={12} className="d-flex">
                        <Grid item xs={5}>
                          Giảm giá:
                        </Grid>
                        <Grid item xs={7}>
                          0
                        </Grid>
                      </Grid>

                      <Grid item xs={12} className="d-flex">
                        <Grid item xs={5}>
                          Tổng tiền thanh toán:
                        </Grid>
                        <Grid item xs={7} style={{ color: "red" }}>
                          {formattedNumber(Total ?? 0)}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </caption>
              </Table>
            </TableContainer>
          </div>
        </div>

        <div className={cx(styles.orderInfoWrapper, "my-4")}>
          <Typography className="mb-4 font-weight-bold">
            3.Lịch sử đơn hàng
          </Typography>
          <div>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className={cx(styles.tableHead)}>#</TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Trạng thái
                    </TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Người gửi
                    </TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Ghi chú
                    </TableCell>
                    <TableCell className={cx(styles.tableHead)} align="center">
                      Thời gian
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody style={{ border: "1px solid #ccc" }}>
                  {listOfOrder.map(row => (
                    <TableRow
                      key={row.order}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {row.order}
                      </TableCell>
                      <TableCell align="center">{row.status}</TableCell>
                      <TableCell align="center">{row.sender}</TableCell>
                      <TableCell align="center">{row.note}</TableCell>
                      <TableCell align="center">{row.time}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default OrderComplete;
