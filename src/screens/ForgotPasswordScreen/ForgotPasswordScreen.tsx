/* eslint-disable jsx-a11y/label-has-associated-control */
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Container, Grid, Typography } from "@mui/material";
import cx from "classnames";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

import { ForgotPassSchema } from "@app/helpers/schema";

import styles from "./ForgotPasswordScreen.module.scss";

const ForgotPasswordScreen = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(ForgotPassSchema),
  });

  const navigate = useNavigate();

  return (
    <div>
      <Container className={cx(styles.root, "my-9")} maxWidth="lg">
        <Grid container>
          <Grid item xs={4}>
            <div className={cx(styles.imgBackground)}>
              <img
                src="https://hoanghamobile.com/Content/web/img/login-bg.png"
                alt="login back ground"
                width="100%"
              />
            </div>
          </Grid>
          <Grid item xs={8}>
            <div className={cx(styles.formWrapper)}>
              <Typography className="font-24 font-weight-bold mb-4">
                Cấp lại mật khẩu
              </Typography>

              <form
                onSubmit={handleSubmit(data => {
                  console.log(data);
                  navigate("/");
                })}
              >
                <label className="font-14 font-weight-bold">
                  Email của bạn
                </label>
                <input
                  {...register("email")}
                  placeholder="Nhập địa chỉ email"
                />
                {errors?.email && (
                  <p className={cx(styles.errorText)}>
                    {errors.email?.message}
                  </p>
                )}

                <Grid container spacing={3} className="flex-justify-end">
                  <Grid item xs={6}>
                    <Button
                      className="flex-justify-center"
                      type="submit"
                      variant="contained"
                      style={{
                        background:
                          "linear-gradient(180deg,#00917a 0%,#00483d 100%)",
                        maxHeight: 41,
                      }}
                    >
                      CẤP LẠI MẬT KHẨU
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Grid>
          <Grid />
        </Grid>
      </Container>
    </div>
  );
};

export default ForgotPasswordScreen;
