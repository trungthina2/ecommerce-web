import React from "react";

import { Container, Typography } from "@mui/material";
import cx from "classnames";

import styles from "./FavoriteScreen.module.scss";

const FavoriteScreen = () => {
  return (
    <Container className={cx(styles.root)}>
      <div className="mb-4">
        <Typography className="font-24 font-weight-bold mb-6">
          Sản phẩm yêu thích
        </Typography>
        <div className={cx(styles.banner, "flex-align-center")}>
          <div>
            <Typography
              className="font-weight-bold font-20"
              style={{ color: "#47637D" }}
            >
              CHÀO MỪNG QUAY TRỞ LẠI
            </Typography>
            <Typography
              className="font-13 my-3"
              style={{ fontStyle: "italic" }}
            >
              Xem và kiểm tra những sản phẩm yêu thích của bạn tại đây
            </Typography>
          </div>
          <div>
            <img
              src="https://hoanghamobile.com/Content/web/content-icon/icon-account-wishlist.png"
              alt="banner"
            />
          </div>
        </div>
      </div>

      <Typography className="font-weight-bold">
        Nội dung đang xây dựng
      </Typography>
    </Container>
  );
};

export default FavoriteScreen;
