/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useEffect, useState } from "react";

import { yupResolver } from "@hookform/resolvers/yup";
import { Container, Grid } from "@mui/material";
import cx from "classnames";
import n2words from "n2words";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

import { CITIES } from "@app/constants/city.constants";
import { CartInfomationSchema, UserInfoSchema } from "@app/helpers/schema";
import { formattedNumber } from "@app/helpers/ultils";

import styles from "./CartScreen.module.scss";

const CartScreen = () => {
  const [listCart, setListCart] = useState<any>([]);
  const [selectedReceive, setSelectedReceive] = useState("1");
  const [listStore, setListStore] = useState<any>([]);
  const [listDistrict, setListDistrict] = useState<any>([]);
  const [userAddress, setUserAddress] = useState<any>({
    city: "",
    district: "",
    address: "",
    note: "",
  });
  const [storeOder, setStoreOrder] = useState<any>({
    city: "",
    store: "",
    note: "",
  });

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(CartInfomationSchema),
  });

  useEffect(() => {
    const cartItemsJson = localStorage.getItem("cartItems");
    if (cartItemsJson) {
      let cartItems: any[] = [];
      cartItems = [...JSON.parse(cartItemsJson as string)];
      setListCart(cartItems);
    }
  }, []);

  const handleIncrease = (index: number) => {
    const newListCards = [...listCart];
    newListCards[index].quantity += 1;
    setListCart(newListCards);
    localStorage.setItem("cartItems", JSON.stringify(newListCards));
  };

  const handleDecrease = (index: number) => {
    const newListCards = [...listCart];
    if (newListCards[index].quantity > 1) {
      newListCards[index].quantity -= 1;
      setListCart(newListCards);
    } else {
      newListCards.splice(index, 1);
      setListCart(newListCards);
    }
    localStorage.setItem("cartItems", JSON.stringify(newListCards));
  };

  const handleChangeQuantity = (event: any, index: number) => {
    const value = parseInt(event.target.value, 10);
    // eslint-disable-next-line no-restricted-globals
    if (!isNaN(value)) {
      const newListCards = [...listCart];
      newListCards[index].quantity = value;
      setListCart(newListCards);
      localStorage.setItem("cartItems", JSON.stringify(newListCards));
    }
  };

  const handleReceiveChange = (event: any) => {
    setUserAddress({
      city: "",
      district: "",
      address: "",
      note: "",
    });
    setStoreOrder({
      city: "",
      store: "",
      note: "",
    });
    setSelectedReceive(event.target.value);
  };

  useEffect(() => {
    if (storeOder.city) {
      const list = CITIES.filter(item => item.name === storeOder.city)[0]
        .stores;
      setListStore(list);
    }
  }, [storeOder.city]);

  useEffect(() => {
    if (userAddress.city) {
      const list = CITIES.filter(item => item.name === userAddress.city)[0]
        .districts;
      setListDistrict(list);
    }
  }, [userAddress.city]);

  const handleChangeInfoReceive = (e: any) => {
    const { name, value } = e.target;
    if (selectedReceive === "1") {
      setUserAddress((prevValues: any) => ({
        ...prevValues,
        [name]: value,
      }));
    } else {
      setStoreOrder((prevValues: any) => ({
        ...prevValues,
        [name]: value,
      }));
    }
  };

  const handleSubmitOrder = async (data: any) => {
    try {
      const userRecieveInfomation = {
        fullname: data.fullname,
        phone: data.phone,
        email: data.email,
        city: userAddress.city,
        district: userAddress.district,
        address: userAddress.address,
        note: userAddress.note,
        deliveryForm: selectedReceive,
        productIds: listCart?.map((item: any) => item.id),
      };

      await UserInfoSchema(selectedReceive).validate(userRecieveInfomation, {
        abortEarly: false,
      });

      await navigate("/order", { state: { userRecieveInfomation, listCart } });

      await localStorage.removeItem("cartItems");
    } catch (error) {
      // eslint-disable-next-line no-console
      // eslint-disable-next-line @typescript-eslint/no-shadow
      const errors: any = {};
      if (error?.inner) {
        // eslint-disable-next-line @typescript-eslint/no-shadow
        error.inner.forEach((error: any) => {
          errors[error.path] = error.message;
        });
      }
    }
  };

  const totalPrice = () => {
    let total = 0;
    listCart.forEach((item: any) => {
      total += parseInt(item.quantity, 10) * parseInt(item.price, 10);
    });
    return total;
  };

  return (
    <Container maxWidth="lg" className={cx(styles.root)}>
      <div className={cx(styles.cart)}>
        <div className={cx(styles.cart_layout)}>
          <div className={cx(styles.cart_info)}>
            {listCart.map((item: any, index: number) => {
              return (
                <div className={cx(styles.cart_items)} key={item.id}>
                  <div className={cx(styles.item)}>
                    <div className={cx(styles.img)}>
                      <img src={item.image} alt={item.name} />
                      <p className={cx(styles.title)}>{item.name}</p>
                      <p className={cx(styles.price)}>
                        <strong>{formattedNumber(item.price)}</strong>
                      </p>
                      <div className={cx(styles.number)}>
                        <label>Số lượng</label>
                        <div className={cx(styles.control)}>
                          <button
                            onClick={() => handleDecrease(index)}
                            className={cx(styles.btn_decrease)}
                          >
                            -
                          </button>
                          <input
                            type="number"
                            value={item.quantity}
                            onChange={(e: any) =>
                              handleChangeQuantity(e, index)
                            }
                            className={cx(styles.input_quantity)}
                          />
                          <button
                            onClick={() => handleIncrease(index)}
                            className={cx(styles.btn_increase)}
                          >
                            +
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className={cx(styles.info)}>
                      <div className={cx(styles.options)}>
                        <div className={cx(styles.option)}>
                          <strong>Phiên bản</strong>
                          <label>
                            <i className="icon-checked" />
                            <span>128GB</span>
                          </label>
                        </div>

                        <div className={cx(styles.option)}>
                          <strong>Màu sắc</strong>
                          <label>
                            <i className="icon-checked" />
                            <span>Blue</span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
            <div className={cx(styles.cart_total)}>
              <p>
                Tổng giá trị:{" "}
                <strong className={cx(styles.price)}>
                  {formattedNumber(totalPrice())}
                </strong>{" "}
              </p>
              <p>
                Tổng thanh toán:{" "}
                <strong className={cx(styles.price, styles.text_red)}>
                  {formattedNumber(totalPrice())}
                </strong>
              </p>
              <p>
                <i>{n2words(totalPrice(), { lang: "vi" })} </i>
              </p>
              <button className={cx(styles.next)}>Tiếp tục</button>
            </div>
          </div>
          <div className={cx(styles.cart_form)}>
            <h3>Thông tin đặt hàng</h3>
            <p className={cx(styles.text_gray)}>
              <i>Bạn cần nhập đầy đủ các trường thông tin có dấu *</i>
            </p>
            <form
              onSubmit={handleSubmit(data => {
                handleSubmitOrder(data);
              })}
            >
              <div className={styles.row}>
                <div className={styles.col}>
                  <div className={styles.control}>
                    <input
                      {...register("fullname")}
                      type="text"
                      placeholder="Họ và tên *"
                    />
                  </div>
                </div>
              </div>
              {errors?.fullname && (
                <p className={cx(styles.errorText)}>
                  {errors.fullname?.message}
                </p>
              )}
              <div className={styles.row}>
                <div className={styles.col}>
                  <div className={styles.control}>
                    <input
                      {...register("phone")}
                      placeholder="Số điện thoại *"
                      type="text"
                    />
                  </div>
                </div>
              </div>
              {errors?.phone && (
                <p className={cx(styles.errorText)}>{errors.phone?.message}</p>
              )}
              <div className={styles.row}>
                <div className={styles.col}>
                  <div className={styles.control}>
                    <input
                      {...register("email")}
                      type="email"
                      placeholder="Email *"
                    />
                  </div>
                </div>
              </div>
              {errors?.email && (
                <p className={cx(styles.errorText)}>{errors.email?.message}</p>
              )}
              <Grid container spacing={2} className={cx(styles.receive)}>
                <Grid item xs={12}>
                  <label> Hình thức nhận hàng</label>
                </Grid>
                <Grid item xs={6}>
                  <div
                    id="payType_1"
                    className={cx(
                      styles.payment_opt,
                      selectedReceive === "1" ? styles.checked : null
                    )}
                  >
                    <label className={cx(styles.radio_ctn)} htmlFor="radio1">
                      <div className={cx(styles.radiobox)}>
                        <input
                          type="radio"
                          id="radio1"
                          name="myRadio"
                          value="1"
                          checked={selectedReceive === "1"}
                          onChange={handleReceiveChange}
                        />
                        <label htmlFor="radio1" />
                      </div>
                      <span>Nhận hàng tại nhà</span>
                    </label>
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div
                    id="payType_1"
                    className={cx(
                      styles.payment_opt,
                      selectedReceive === "2" ? styles.checked : null
                    )}
                  >
                    <label className={cx(styles.radio_ctn)} htmlFor="radio2">
                      <div className={cx(styles.radiobox)}>
                        <input
                          type="radio"
                          id="radio2"
                          name="myRadio"
                          value="2"
                          checked={selectedReceive === "2"}
                          onChange={handleReceiveChange}
                        />
                        <label htmlFor="radio2" />
                      </div>
                      <span>Nhận hàng tại cửa hàng</span>
                    </label>
                  </div>
                </Grid>
              </Grid>
              <Grid
                container
                spacing={1}
                className={cx(styles.delivery_address)}
              >
                <Grid item xs={12}>
                  <label>
                    {selectedReceive === "1" ? "Địa chỉ" : "Nơi nhận hàng"}
                  </label>
                </Grid>
                {selectedReceive === "1" ? (
                  <>
                    <Grid item xs={6}>
                      <div className={styles.row}>
                        <div className={styles.col}>
                          <div className={styles.control}>
                            <select
                              name="city"
                              id="city"
                              onChange={handleChangeInfoReceive}
                            >
                              <option value="">Tỉnh/Thành phố *</option>

                              {CITIES.map(item => {
                                const { name } = item;
                                return (
                                  <option key={name} value={name}>
                                    {name}
                                  </option>
                                );
                              })}
                            </select>
                          </div>
                        </div>
                      </div>

                      {/* {errors?.city && (
                      <p className={cx(styles.error_text)}>{errors?.city}</p>
                    )} */}
                    </Grid>
                    <Grid item xs={6}>
                      <div className={styles.row}>
                        <div className={styles.col}>
                          <div className={styles.control}>
                            <select
                              name="district"
                              id="district"
                              onChange={handleChangeInfoReceive}
                            >
                              <option value="">Quận/Huyện *</option>

                              {listDistrict.map((item: any) => (
                                <option key={item} value={item}>
                                  {item}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* {errors?.district && (
                      <p className={cx(styles.error_text)}>
                        {errors?.district}
                      </p>
                    )} */}
                    </Grid>
                    <Grid item xs={12}>
                      <div className={styles.row}>
                        <div className={styles.col}>
                          <div className={styles.control}>
                            <input
                              type="text"
                              name="address"
                              placeholder="Địa chỉ nhận hàng *"
                              onChange={handleChangeInfoReceive}
                            />
                          </div>
                        </div>
                      </div>
                      {/* {errors?.address && (
                      <p className={cx(styles.error_text)}>{errors?.address}</p>
                    )} */}
                    </Grid>
                    <Grid item xs={12}>
                      <div className={styles.row}>
                        <div className={styles.col}>
                          <div className={styles.control}>
                            <textarea
                              name="note"
                              placeholder="Ghi chú"
                              onChange={handleChangeInfoReceive}
                            />
                          </div>
                        </div>
                      </div>
                      {/* {errors?.note && (
                      <p className={cx(styles.error_text)}>{errors?.note}</p>
                    )} */}
                    </Grid>
                  </>
                ) : (
                  <>
                    <Grid item xs={6}>
                      <div className={styles.row}>
                        <div className={styles.col}>
                          <div className={styles.control}>
                            <select
                              name="city"
                              id="city"
                              onChange={handleChangeInfoReceive}
                            >
                              <option value="">Tỉnh/Thành phố *</option>
                              {CITIES.map(item => {
                                const { name } = item;
                                return (
                                  <option key={name} value={name}>
                                    {name}
                                  </option>
                                );
                              })}
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* {errors?.city && (
                      <p className={cx(styles.error_text)}>{errors?.city}</p>
                    )} */}
                    </Grid>
                    <Grid item xs={6}>
                      <div className={styles.row}>
                        <div className={styles.col}>
                          <div className={styles.control}>
                            <select
                              name="store"
                              id="district"
                              onChange={handleChangeInfoReceive}
                            >
                              <option value="">Cửa hàng *</option>
                              {listStore.map((item: any) => (
                                <option key={item} value={item}>
                                  {item}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* {errors?.store && (
                      <p className={cx(styles.error_text)}>{errors?.store}</p>
                    )} */}
                    </Grid>
                    <Grid item xs={12}>
                      <div className={styles.row}>
                        <div className={styles.col}>
                          <div className={styles.control}>
                            <textarea
                              name="note"
                              placeholder="Ghi chú"
                              onChange={handleChangeInfoReceive}
                            />
                          </div>
                        </div>
                      </div>
                      {/* {errors?.note && (
                      <p className={cx(styles.error_text)}>{errors?.note}</p>
                    )} */}
                    </Grid>
                  </>
                )}
                <Grid
                  item
                  xs={12}
                  className={cx(styles.btn_order, "flex-center")}
                  onClick={handleSubmitOrder}
                >
                  <button type="submit">TIẾN HÀNH ĐẶT HÀNG</button>
                </Grid>
              </Grid>
            </form>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default CartScreen;
