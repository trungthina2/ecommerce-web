/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/label-has-associated-control */

import { yupResolver } from "@hookform/resolvers/yup";
import {
  Button,
  Checkbox,
  Container,
  Divider,
  Grid,
  Typography,
} from "@mui/material";
import cx from "classnames";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";

import firebaseInstance from "@app/config/firebase";
import { LoginSchema } from "@app/helpers/schema";
import { setIsLogedIn } from "@app/redux/example.slice";
import store from "@app/redux/store";

import styles from "./LoginScreen.module.scss";

const LoginScreen = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(LoginSchema),
  });

  const navigate = useNavigate();

  const handleLogin = (data: any) => {
    firebaseInstance
      .signIn(data.username, data.password)
      .then((loginData: any) => {
        localStorage.setItem("userId", loginData.user?.multiFactor?.user.uid);
        localStorage.setItem(
          "userEmail",
          loginData.user?.multiFactor?.user.email
        );
      })
      .then(() => store.dispatch(setIsLogedIn()))
      .then(() => navigate("/"))
      .catch((error: any) => {
        throw error;
      });
  };
  return (
    <div>
      <Container className={cx(styles.root, "my-9")} maxWidth="lg">
        <Grid container className={cx(styles.form)}>
          <Grid item xs={4}>
            <div className={cx(styles.imgBackground)}>
              <img
                src="https://hoanghamobile.com/Content/web/img/login-bg.png"
                alt="login back ground"
                width="100%"
              />
            </div>
          </Grid>
          <Grid item xs={8} className={cx(styles.formBox)}>
            <div className={cx(styles.formWrapper)}>
              <Typography className="font-24 font-weight-bold mb-4">
                Đăng nhập
              </Typography>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <Button
                    variant="contained"
                    style={{ backgroundColor: "#4267b2" }}
                  >
                    <img
                      className="mr-2"
                      src="https://hoanghamobile.com/Content/web/img/login-facebook.png"
                      alt="facebook logo"
                    />
                    Tiếp tục với FaceBook
                  </Button>
                </Grid>
                <Grid item xs={6}>
                  <Button
                    variant="contained"
                    style={{ backgroundColor: "#4385f5" }}
                  >
                    <img
                      className="mr-2"
                      src="https://hoanghamobile.com/Content/web/img/login-google.png"
                      alt="google logo"
                    />
                    Tiếp tục với Google
                  </Button>
                </Grid>
              </Grid>
              <Divider className="my-5" orientation="horizontal" flexItem>
                Hoặc
              </Divider>
              <form
                onSubmit={handleSubmit(data => {
                  handleLogin(data);
                })}
              >
                <label className="font-14 font-weight-bold">Tài khoản</label>
                <input {...register("username")} />
                {errors?.username && (
                  <p className={cx(styles.errorText)}>
                    {errors.username?.message}
                  </p>
                )}
                <label className="font-14 font-weight-bold">Mật khẩu</label>
                <input {...register("password")} type="password" />
                {errors?.password && (
                  <p className={cx(styles.errorText)}>
                    {errors.password?.message}
                  </p>
                )}
                <div className="mb-3">
                  <Checkbox className="p-0" {...register("remember_me")} />
                  <label>Nhớ đăng nhập</label>
                </div>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Button
                      className="flex-justify-center"
                      type="submit"
                      variant="contained"
                      style={{
                        background:
                          "linear-gradient(180deg,#00917a 0%,#00483d 100%)",
                        maxHeight: 41,
                      }}
                    >
                      ĐĂNG NHẬP
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      className="flex-justify-center"
                      variant="contained"
                      style={{
                        backgroundColor: "transparent",
                        color: "black",
                        border: "2px solid black",
                        maxHeight: 41,
                      }}
                      onClick={() => navigate("/register")}
                    >
                      ĐĂNG KÝ
                    </Button>
                  </Grid>
                </Grid>
              </form>
              <Link
                className="flex-justify-end mt-2 font-13 font-weight-medium"
                style={{ color: "#222" }}
                to="/forgot-password"
              >
                Quên mật khẩu?
              </Link>
            </div>
          </Grid>
          <Grid />
        </Grid>
      </Container>
    </div>
  );
};

export default LoginScreen;
