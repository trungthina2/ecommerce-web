import React, { useEffect, useState } from "react";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import RemoveCircleIcon from "@mui/icons-material/RemoveCircle";
import { Container, Grid, Typography } from "@mui/material";
import cx from "classnames";

import {
  CPUItems,
  GraphicCard,
  RAM,
  brandItems,
  hardDrive,
  price,
  resolution,
  size,
  sort,
  typeItems,
} from "@app/constants/filter.constant";
import { FAKE_PRODUCT } from "@app/constants/product-detail.constants";

import styles from "./ProductsScreen.module.scss";

const ProductsScreen = () => {
  const [cpu, setCpu] = useState<string>("");
  const [brand, setBrand] = useState<string>("");
  const [prices, setPrices] = useState<any>({
    title: undefined,
    from: 0,
    to: 1000000000000000,
  });
  const [category, setCategory] = useState<string>("");
  const [resolutions, setResolutions] = useState<string>("");
  const [screenSize, setScreenSize] = useState("");
  const [ram, setRam] = useState("");
  const [graphicCard, setGraphicCard] = useState("");
  const [hardDrives, setHardDrive] = useState("");
  const [sorts, setSorts] = useState("");

  const [sortedData, setSortedData] = useState<any>();
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    const datas = sorts === "" ? FAKE_PRODUCT : sortedData;
    const filteredData = datas.filter((item: any) => {
      return (
        (cpu === "" || item.cpu === cpu) &&
        (brand === "" || item.brand === brand) &&
        (category === "" || item.type === category) &&
        (resolutions === "" || item.resolution === resolutions) &&
        (screenSize === "" || item.screen_size === screenSize) &&
        (ram === "" || item.ram === ram) &&
        (graphicCard === "" || item.card === graphicCard) &&
        (hardDrives === "" || item.SSD === hardDrives) &&
        item.price >= prices?.from &&
        item.price <= prices?.to
      );
    });

    setData(filteredData);
  }, [
    cpu,
    brand,
    category,
    resolutions,
    screenSize,
    ram,
    graphicCard,
    hardDrives,
    prices?.from,
    prices?.to,
    sorts,
    sortedData,
    prices.title,
  ]);

  useEffect(() => {
    const cpuFilter = sessionStorage.getItem("cpu");
    if (cpuFilter) {
      setCpu(cpuFilter);
    }

    const brandFilter = sessionStorage.getItem("brand");
    if (brandFilter) {
      setBrand(brandFilter);
    }

    const ramFilter = sessionStorage.getItem("ram");
    if (ramFilter) {
      setBrand(ramFilter);
    }

    const priceFilter = sessionStorage.getItem("prices");

    if (priceFilter) {
      setPrices(JSON.parse(priceFilter));
    }

    const categoryFilter = sessionStorage.getItem("category");
    if (categoryFilter) {
      setCategory(categoryFilter);
    }

    const resolutionFilter = sessionStorage.getItem("resolutions");
    if (resolutionFilter) {
      setResolutions(resolutionFilter);
    }

    const sizeFilter = sessionStorage.getItem("screenSize");
    if (sizeFilter) {
      setScreenSize(sizeFilter);
    }

    const graphicCardFilter = sessionStorage.getItem("graphicCard");
    if (graphicCardFilter) {
      setGraphicCard(graphicCardFilter);
    }

    const hardDriveFilter = sessionStorage.getItem("hardDrives");
    if (hardDriveFilter) {
      setHardDrive(hardDriveFilter);
    }

    const sortFilter = sessionStorage.getItem("sorts");
    if (sortFilter) {
      setSorts(sortFilter);
    }
  }, []);

  useEffect(() => {
    setSortedData(FAKE_PRODUCT);
  }, [data]);

  const handleSort = (value: number) => {
    const dataSort =
      cpu !== "" ||
      brand !== "" ||
      category !== "" ||
      resolutions !== "" ||
      screenSize !== "" ||
      ram !== "" ||
      graphicCard !== "" ||
      hardDrives !== "" ||
      prices.title !== undefined
        ? data
        : FAKE_PRODUCT;

    switch (value) {
      case 1:
        setSortedData(dataSort);
        return;
      case 3:
        setSortedData(dataSort.sort((a: any, b: any) => a.price - b.price));
        return;
      case 4:
        setSortedData(dataSort.sort((a: any, b: any) => b.price - a.price));
        return;
      // case 5:
      //   setData(data.sort((a: any, b: any) => a.modifyAt - b.modifyAt));
      //   return;
      // case 6:
      //   setData(data.sort((a: any, b: any) => b.createAt - a.createAt));
      //   return;
      // case 7:
      //   setData(data.sort((a: any, b: any) => a.mostViewDay - b.mostViewDay));
      //   return;
      // case 8:
      //   setData(data.sort((a: any, b: any) => a.mostViewWeek - b.mostViewWeek));
      //   return;
      // case 9:
      //   setData(
      //     data.sort((a: any, b: any) => a.mostViewMonth - b.mostViewMonth)
      //   );
      //   return;
      // case 10:
      //   setData(data.sort((a: any, b: any) => a.mostViewYear - b.mostViewYear));
      //   return;
      // case 11:
      //   setData(data.sort((a: any, b: any) => a.mostView - b.mostView));
      //   return;
      // case 2:
      //   setData(data.sort((a: any, b: any) => a.createAt - b.createAt));
      //   return;
      default:
        setData(data);
    }
  };

  return (
    <Container maxWidth="lg" className="p-0 mb-4">
      <div className={cx(styles.filterWrapper)}>
        <Grid
          container
          className="font-13 flex-align-center flex-space-between"
          spacing={1}
        >
          <Grid item xs={1}>
            <Typography
              className="font-13 font-weight-bold"
              style={{ color: "#00917a" }}
            >
              Lọc danh sách:
            </Typography>
          </Grid>

          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={cpu ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  setCpu("");
                  sessionStorage.setItem("cpu", "");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">
                <span>Dòng CPU</span>
                <span className={cx(styles.filterText)}>{`:${cpu}`}</span>
              </span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {CPUItems.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setCpu(item);
                        sessionStorage.setItem("cpu", item);
                      }}
                    >
                      {item}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={brand ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  setBrand("");
                  sessionStorage.setItem("brand", "");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">Danh mục</span>
              <span className={cx(styles.filterText)}>{`:${brand}`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {brandItems.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setBrand(item);
                        sessionStorage.setItem("brand", item);
                      }}
                    >
                      {item}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={
                  prices.title === undefined || prices.title === null
                    ? "d-none"
                    : "cursor-pointer"
                }
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  sessionStorage.setItem(
                    "prices",
                    JSON.stringify({
                      title: undefined,
                      from: 0,
                      to: 1000000000000000,
                    })
                  );
                  setPrices({
                    title: undefined,
                    from: 0,
                    to: 1000000000000000,
                  });
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">Giá</span>
              <span className={cx(styles.filterText)}>{`:${
                prices?.title ?? ""
              }`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {price.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setPrices(item);
                        sessionStorage.setItem("prices", JSON.stringify(item));
                      }}
                    >
                      {item.title}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={category ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  setCategory("");
                  sessionStorage.setItem("category", "");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">Loại sản phẩm</span>
              <span className={cx(styles.filterText)}>{`:${category}`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {typeItems.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setCategory(item);
                        sessionStorage.setItem("category", item);
                      }}
                    >
                      {item}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={resolutions ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  setResolutions("");
                  sessionStorage.setItem("resolutions", "");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">Độ phân giải</span>
              <span className={cx(styles.filterText)}>{`:${resolutions}`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {resolution.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setResolutions(item);
                        sessionStorage.setItem("resolutions", item);
                      }}
                    >
                      {item}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={screenSize ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  setScreenSize("");
                  sessionStorage.setItem("screenSize", "");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">Kích thước màn hình</span>
              <span className={cx(styles.filterText)}>{`:${screenSize}`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {size.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setScreenSize(item);
                        sessionStorage.setItem("screenSize", item);
                      }}
                    >
                      {item}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={ram ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  setRam("");
                  sessionStorage.setItem("ram", "");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">RAM</span>
              <span className={cx(styles.filterText)}>{`:${ram}`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {RAM.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setRam(item);
                        sessionStorage.setItem("ram", item);
                      }}
                    >
                      {item}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={graphicCard ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  setGraphicCard("");
                  sessionStorage.setItem("graphicCard", "");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">Card đồ họa rời</span>
              <span className={cx(styles.filterText)}>{`:${graphicCard}`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {GraphicCard.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setGraphicCard(item);

                        sessionStorage.setItem("graphicCard", item);
                      }}
                    >
                      {item}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={hardDrives ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  setHardDrive("");
                  sessionStorage.setItem("hardDrives", "");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">Ổ cứng mặc định</span>
              <span className={cx(styles.filterText)}>{`:${hardDrives}`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {hardDrive.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        setHardDrive(item);
                        sessionStorage.setItem("hardDrives", item);
                      }}
                    >
                      {item}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
          <Grid className={cx(styles.filterItemWrapper, "font-13")} item xs={1}>
            <div className="flex-justify-center">
              <button
                className={sorts ? "cursor-pointer" : "d-none"}
                style={{ width: 20, height: 20, padding: 0 }}
                onClick={() => {
                  handleSort(1);
                  sessionStorage.setItem("sorts", "");
                  setSorts("");
                }}
              >
                <RemoveCircleIcon
                  className="font-14"
                  style={{ color: "red" }}
                />
              </button>
              <span className="font-11">Sắp xếp</span>
              <span className={cx(styles.filterText)}>{`:${sorts}`}</span>
              <KeyboardArrowDownIcon className="font-14" />
            </div>
            <div className={cx(styles.subFilter)}>
              <Grid container spacing={2}>
                {sort.map(item => (
                  <Grid item xs={1} className={cx(styles.item)}>
                    <button
                      onClick={() => {
                        handleSort(item.value);
                        setSorts(item.title);
                        sessionStorage.setItem("sorts", item.title);
                      }}
                    >
                      {item.title}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </div>
          </Grid>
        </Grid>
      </div>
      <Grid
        container
        className={cx(styles.list_product_container, "mt-4")}
        spacing={1}
        columns={10}
      >
        {data.map((item: any) => (
          <Grid item xs={2} key={item.id} className={styles.item}>
            <div className={cx(styles.item_product_container)}>
              <div className={cx(styles.item_product_header)}>
                <img
                  src={item.image}
                  alt=""
                  className={cx(styles.img_product)}
                />
                <div className={cx(styles.promote)}>
                  <a href={"/product/:id".replace(":id", item.id.toString())}>
                    <ul>
                      <li>
                        <span className={cx(styles.bag)}>KM</span> Giảm thêm tới
                        1,5 triệu khi đăng ký gói cước tích điểm MobiFone.
                      </li>
                      <li>
                        <span className={cx(styles.bag)}>KM</span> Giảm tới
                        150.000đ khi thanh toán qua VNPAY-QR (Áp dụng cho đơn
                        hàng trên 3.000.000đ).
                      </li>
                      <li>
                        <span className={cx(styles.bag)}>KM</span> Giảm thêm tới
                        1.000.000đ khi Thu cũ - Lên đời iPhone 11 | 12 | 13
                        Series
                      </li>
                    </ul>
                  </a>
                </div>
              </div>
              <div className={cx(styles.item_product_content)}>
                <a
                  href={"/product/:id".replace(":id", item.id.toString())}
                  className={cx(styles.title)}
                >
                  {item.name}
                </a>
                <div className={cx(styles.price)}>
                  <strong>{item.price}</strong>
                  <small>{item.cost}</small>
                </div>
                <div className={cx(styles.up_form)}>
                  <p>
                    Giá lên đời từ:
                    <strong className={cx(styles.text_red)}>
                      {item.subsidy}
                    </strong>
                  </p>
                </div>
              </div>
              <div className={cx(styles.note)}>
                <span className={cx(styles.bag)}>KM</span>{" "}
                <span title="Giảm thêm tới 1,5 triệu khi đăng ký gói cước tích điểm MobiFone.">
                  Giảm thêm tới 1,5 triệu khi đăng ký...
                </span>
                <strong className={cx(styles.text_orange)}>
                  VÀ 10 KM KHÁC
                </strong>
              </div>
            </div>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default ProductsScreen;
