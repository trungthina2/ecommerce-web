import React from "react";

import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import PlaceIcon from "@mui/icons-material/Place";
import SearchIcon from "@mui/icons-material/Search";
import { Button, Container, Grid, Typography } from "@mui/material";
import cx from "classnames";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

import styles from "./ServiceScreen.module.scss";
import serviceImg from "../../assets/images/care-services.png";

const ServiceScreen = () => {
  const { register, handleSubmit } = useForm();
  const navigate = useNavigate();

  const handleSearch = (value: string) => {
    console.log(value);
  };

  return (
    <Container maxWidth="lg">
      <div className={cx(styles.root)}>
        <button
          className="d-flex flex-align-center cursor-pointer"
          onClick={() => navigate("/")}
        >
          <ArrowBackIosIcon className="font-40 font-weight-normal" />
          <Typography>Trở về trang chủ</Typography>
        </button>
        <Grid container className="flex-direction.column flex-center">
          <Grid item xs={6}>
            <img src={serviceImg} width="100%" alt="service" />
            <Typography className="font-15 my-5 text-center font-weight-bold">
              Tra cứu bảo hành sản phẩm
            </Typography>
            <form
              style={{ position: "relative" }}
              onSubmit={handleSubmit((data: any) => {
                console.log(data);
              })}
            >
              <input
                className={cx(styles.searchBox, "font-14")}
                placeholder="Nhập IMEI sản phẩm hoặc mã phiếu bảo hành cần tra cứu"
                {...register("searchkey")}
                onChange={e => handleSearch(e.target.value)}
              />
              <button className={cx(styles.searhBtn)} type="submit">
                <SearchIcon />
              </button>
            </form>

            <Typography className="font-15 font-weight-bold mt-7 mb-4 text-center">
              Hệ thống bảo hành điện thoại toàn quốc
            </Typography>
            <Button
              className={cx(styles.placeBtn, "font-15 font-weight-bold")}
              startIcon={<PlaceIcon />}
            >
              LỰA CHỌN HÃNG ĐIỆN THOẠI & NHÀ PHÂN PHỐI
            </Button>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default ServiceScreen;
