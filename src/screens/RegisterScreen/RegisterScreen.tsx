/* eslint-disable jsx-a11y/label-has-associated-control */

import { useEffect, useState } from "react";

import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Container, Grid, Radio, Typography } from "@mui/material";
import cx from "classnames";
import { useForm } from "react-hook-form";

import { CITIES } from "@app/constants/city.constants";
import { SignupSchema } from "@app/helpers/schema";

import styles from "./RegisterScreen.module.scss";

const RegisterScreen = () => {
  const [gender, setGender] = useState<string>("");
  const [listDistrict, setListDistrict] = useState<any>([]);
  const [cityName, setCityName] = useState<string>("");

  useEffect(() => {
    if (cityName !== "") {
      const list = CITIES.filter(item => item.name === cityName)[0].districts;
      setListDistrict(list);
    }
  }, [cityName]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(SignupSchema),
  });

  const handleChangeGender = (e: any) => {
    setGender(e.target.value);
  };

  return (
    <div>
      <Container className={cx(styles.root, "my-9")} maxWidth="lg">
        <Grid container className={styles.form}>
          <Grid item xs={4}>
            <div className={cx(styles.imgBackground, "flex-align-center")}>
              <img
                src="https://hoanghamobile.com/Content/web/img/login-bg.png"
                alt="login back ground"
                width="100%"
              />
            </div>
          </Grid>
          <Grid item xs={8} className={styles.formBox}>
            <div className={cx(styles.formWrapper)}>
              <Typography className="font-24 font-weight-bold mb-3 text-center">
                Đăng ký tài khoản
              </Typography>
              <Typography className="font-13 mb-4 text-center">
                Chú ý các nội dung có dấu * bạn cần phải nhập
              </Typography>

              <form
                onSubmit={handleSubmit(data => {
                  console.log({ gender, ...data });
                })}
              >
                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">
                      Tài khoản :
                    </label>
                  </Grid>
                  <Grid item xs={8}>
                    <input
                      placeholder="Tài khoản*"
                      {...register("username")}
                      required
                    />
                    {errors?.username && (
                      <p className={cx(styles.errorText)}>
                        {errors.username?.message}
                      </p>
                    )}
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">Họ tên:</label>
                  </Grid>
                  <Grid item xs={8}>
                    <input
                      placeholder="Họ tên*"
                      {...register("fullname")}
                      required
                    />
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">
                      Mật khẩu:
                    </label>
                  </Grid>
                  <Grid item xs={8}>
                    <input
                      placeholder="Mật khẩu"
                      type="password"
                      {...register("password", {
                        minLength: 6,
                      })}
                      required
                    />
                    {errors?.password && (
                      <p className={cx(styles.errorText)}>
                        {errors.password?.message}
                      </p>
                    )}
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">
                      Nhập lại mật khẩu:
                    </label>
                  </Grid>
                  <Grid item xs={8}>
                    <input
                      placeholder="Nhập lại mật khẩu*"
                      type="password"
                      {...register("password2")}
                      required
                    />
                    {errors?.password2 && (
                      <p className={cx(styles.errorText)}>
                        {errors.password2?.message}
                      </p>
                    )}
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">Email:</label>
                  </Grid>
                  <Grid item xs={8}>
                    <input
                      placeholder="Email*"
                      {...register("email")}
                      type="email"
                      required
                    />
                    {errors?.email && (
                      <p className={cx(styles.errorText)}>
                        {errors.email?.message}
                      </p>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">
                      Giới tính:
                    </label>
                  </Grid>
                  <Grid item xs={8} className="d-flex mb-3">
                    <Grid item xs={6} className="d-flex flex-align-center">
                      <Radio
                        checked={gender === "nam"}
                        className="my-0 mr-4"
                        value="nam"
                        style={{ width: 24, color: "#009981" }}
                        onChange={e => handleChangeGender(e)}
                      />
                      <label
                        className="font-weight-bold"
                        style={{ color: "#858585", fontStyle: "italic" }}
                      >
                        Nam
                      </label>
                    </Grid>
                    <Grid item xs={6} className="d-flex flex-align-center">
                      <Radio
                        checked={gender === "nu"}
                        className="my-0 mr-4"
                        value="nu"
                        style={{ width: 24, color: "#009981" }}
                        onChange={e => handleChangeGender(e)}
                      />
                      <label
                        className="font-weight-bold"
                        style={{ color: "#858585", fontStyle: "italic" }}
                      >
                        Nữ
                      </label>
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={4}>
                      <label className="font-13 font-weight-bold">
                        Ngày tháng năm sinh:
                      </label>
                    </Grid>
                    <Grid item xs={8}>
                      <input
                        type="date"
                        placeholder="Ngày tháng năm sinh"
                        {...register("birthday")}
                      />
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={4}>
                      <label className="font-13 font-weight-bold">
                        Điện thoại:
                      </label>
                    </Grid>
                    <Grid item xs={8}>
                      <input
                        placeholder="Điện thoại*"
                        {...register("phone")}
                        required
                        type="text"
                      />
                      {errors?.phone && (
                        <p className={cx(styles.errorText)}>
                          {errors.phone?.message}
                        </p>
                      )}
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={4}>
                      <label className="font-13 font-weight-bold">
                        Địa chỉ:
                      </label>
                    </Grid>
                    <Grid item xs={8}>
                      <input
                        placeholder="Địa chỉ*"
                        {...register("address")}
                        required
                      />
                      {errors?.address && (
                        <p className={cx(styles.errorText)}>
                          {errors.address?.message}
                        </p>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={4}>
                      <label className="font-13 font-weight-bold">
                        Tỉnh/Thành phố:
                      </label>
                    </Grid>
                    <Grid item xs={8}>
                      <select
                        {...register("city")}
                        className="mb-3"
                        onChange={e => setCityName(e.target.value)}
                      >
                        {CITIES.map((item: any) => {
                          const { name } = item;
                          return (
                            <option key={name} value={name}>
                              {name}
                            </option>
                          );
                        })}
                      </select>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={4}>
                      <label className="font-13 font-weight-bold">
                        Quận/Huyện:
                      </label>
                    </Grid>
                    <Grid item xs={8}>
                      <select {...register("province")} className="mb-3">
                        {listDistrict.map((item: any) => (
                          <option key={item} value={item}>
                            {item}
                          </option>
                        ))}
                      </select>
                    </Grid>
                  </Grid>
                </Grid>

                <Grid container spacing={3} className="flex-justify-center">
                  <Grid item xs={6}>
                    <Button
                      className="flex-justify-center mt-4"
                      type="submit"
                      variant="contained"
                      style={{
                        background:
                          "linear-gradient(180deg,#00917a 0%,#00483d 100%)",
                        maxHeight: 41,
                        borderRadius: "8px",
                      }}
                    >
                      ĐĂNG KÝ TÀI KHOẢN
                    </Button>
                  </Grid>
                </Grid>
                <Typography className="text-center font-13 font-weight-bold my-6">
                  Bằng việc đăng kí này, bạn đã chấp nhận các chính sách của
                  Hoàng Hà Mobile
                </Typography>
              </form>
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default RegisterScreen;
