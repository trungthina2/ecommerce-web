/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from "react";

import { yupResolver } from "@hookform/resolvers/yup";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import { Button, Container, Grid, Radio, Typography } from "@mui/material";
import cx from "classnames";
import { useForm } from "react-hook-form";

import { UpdateUserInfoSchema } from "@app/helpers/schema";

import styles from "./UserInfoScreen.module.scss";
import UserImg from "../../assets/images/icon-account-info.png";

const UserInfoScreen = () => {
  const [gender, setGender] = useState<string>("");

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(UpdateUserInfoSchema),
  });

  const handleChangeGender = (e: any) => {
    setGender(e.target.value);
  };

  return (
    <Container maxWidth="lg" className={cx(styles.root)}>
      <div className="mb-4">
        <Typography className="font-24 font-weight-bold mb-6">
          Thông tin tài khoản
        </Typography>
        <div className={cx(styles.banner, "flex-align-center")}>
          <div>
            <Typography
              className="font-weight-bold font-20"
              style={{ color: "#47637D" }}
            >
              CHÀO MỪNG QUAY TRỞ LẠI
            </Typography>
            <Typography
              className="font-13 my-3"
              style={{ fontStyle: "italic" }}
            >
              Kiểm tra và chỉnh sửa thông tin cá nhân của bạn tại đây
            </Typography>
          </div>
          <div>
            <img src={UserImg} alt="banner" />
          </div>
        </div>
      </div>
      <Grid container spacing={1}>
        <Grid item xs={9}>
          <Typography className="font-weight-bold">
            Cập nhật thông tin cá nhân
          </Typography>
          <div className={cx(styles.infoWrapper, "mt-5")}>
            <form
              onSubmit={handleSubmit(data => {
                console.log({ gender, ...data });
              })}
            >
              <Grid container>
                <Grid item xs={4}>
                  <label className="font-13 font-weight-bold">Họ tên:</label>
                </Grid>
                <Grid item xs={8}>
                  <input placeholder="Họ tên*" {...register("fullname")} />
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={4}>
                  <label className="font-13 font-weight-bold">Giới tính:</label>
                </Grid>
                <Grid item xs={8} className="d-flex mb-3">
                  <Grid item xs={6} className="d-flex flex-align-center">
                    <Radio
                      checked={gender === "nam"}
                      className="my-0 mr-4"
                      value="nam"
                      style={{ width: 24, color: "#009981" }}
                      onChange={e => handleChangeGender(e)}
                    />
                    <label
                      className="font-weight-bold"
                      style={{ color: "#858585", fontStyle: "italic" }}
                    >
                      Nam
                    </label>
                  </Grid>
                  <Grid item xs={6} className="d-flex flex-align-center">
                    <Radio
                      checked={gender === "nu"}
                      className="my-0 mr-4"
                      value="nu"
                      style={{ width: 24, color: "#009981" }}
                      onChange={e => handleChangeGender(e)}
                    />
                    <label
                      className="font-weight-bold"
                      style={{ color: "#858585", fontStyle: "italic" }}
                    >
                      Nữ
                    </label>
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">
                      Điện thoại:
                    </label>
                  </Grid>
                  <Grid item xs={8}>
                    <input placeholder="Điện thoại*" {...register("phone")} />
                    {errors?.phone && (
                      <p className={cx(styles.errorText)}>
                        {errors.phone?.message}
                      </p>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">Email:</label>
                  </Grid>
                  <Grid item xs={8}>
                    <input
                      placeholder="Email*"
                      {...register("email")}
                      type="email"
                    />
                    {errors?.email && (
                      <p className={cx(styles.errorText)}>
                        {errors.email?.message}
                      </p>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">Địa chỉ:</label>
                  </Grid>
                  <Grid item xs={8}>
                    <input placeholder="Địa chỉ*" {...register("address")} />
                    {errors?.address && (
                      <p className={cx(styles.errorText)}>
                        {errors.address?.message}
                      </p>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">
                      Tỉnh/Thành phố:
                    </label>
                  </Grid>
                  <Grid item xs={8}>
                    <select {...register("city")} className="mb-3">
                      <option value="volvo">Volvo</option>
                      <option value="saab">Saab</option>
                      <option value="opel">Opel</option>
                      <option value="audi">Audi</option>
                    </select>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={4}>
                    <label className="font-13 font-weight-bold">
                      Quận/Huyện:
                    </label>
                  </Grid>
                  <Grid item xs={8}>
                    <select {...register("province")} className="mb-3">
                      <option value="volvo">Volvo</option>
                      <option value="saab">Saab</option>
                      <option value="opel">Opel</option>
                      <option value="audi">Audi</option>
                    </select>
                  </Grid>
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={4}>
                  <label className="font-13 font-weight-bold">
                    Ngày tháng năm sinh:
                  </label>
                </Grid>
                <Grid item xs={8}>
                  <input
                    type="date"
                    placeholder="Ngày tháng năm sinh"
                    {...register("birthday")}
                  />
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={4}>
                  <label className="font-13 font-weight-bold">
                    Tên công ty:
                  </label>
                </Grid>
                <Grid item xs={8}>
                  <input placeholder="Tên công ty" {...register("company")} />
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={4}>
                  <label className="font-13 font-weight-bold">
                    Địa chỉ công ty:
                  </label>
                </Grid>
                <Grid item xs={8}>
                  <input
                    placeholder="Địa chỉ công ty"
                    {...register("company_address")}
                  />
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={4}>
                  <label className="font-13 font-weight-bold">
                    Mã số thuế công ty:
                  </label>
                </Grid>
                <Grid item xs={8}>
                  <input
                    placeholder="Mã số thuế công ty"
                    {...register("company_taxcode")}
                  />
                </Grid>
              </Grid>
              <Typography className="my-7 text-center font-13">
                Để trống nếu không muốn thay đổi mật khẩu.
              </Typography>
              <Grid container>
                <Grid item xs={4}>
                  <label className="font-13 font-weight-bold">
                    Mật khẩu mới:
                  </label>
                </Grid>
                <Grid item xs={8}>
                  <input
                    placeholder="Mật khẩu"
                    type="password"
                    {...register("password", {})}
                  />
                  {errors?.password && (
                    <p className={cx(styles.errorText)}>
                      {errors.password?.message}
                    </p>
                  )}
                </Grid>
              </Grid>
              <Grid container>
                <Grid item xs={4}>
                  <label className="font-13 font-weight-bold">
                    Nhập lại mật khẩu mới:
                  </label>
                </Grid>
                <Grid item xs={8}>
                  <input
                    placeholder="Nhập lại mật khẩu*"
                    type="password"
                    {...register("password2")}
                  />
                  {errors?.password2 && (
                    <p className={cx(styles.errorText)}>
                      {errors.password2?.message}
                    </p>
                  )}
                </Grid>
              </Grid>

              <Grid container spacing={3} className="flex-justify-center">
                <Grid item xs={6} className="flex-center">
                  <Button
                    className="flex-justify-center mt-4"
                    type="submit"
                    variant="contained"
                    style={{
                      background:
                        "linear-gradient(180deg,#00917a 0%,#00483d 100%)",
                      maxHeight: 41,
                      borderRadius: "8px",
                    }}
                  >
                    XÁC NHẬN
                  </Button>
                </Grid>
              </Grid>
            </form>
          </div>
        </Grid>

        <Grid item xs={3}>
          <Typography className="font-weight-bold">Tư cách hiển thị</Typography>
          <div
            className={cx(
              styles.infoWrapper,
              "mt-5  flex-align-center flex-direction-column "
            )}
          >
            <div
              className="flex-align-center flex-direction-column full-width"
              style={{ paddingBottom: 30, borderBottom: "1px solid #ccc" }}
            >
              <div
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 18,
                  overflow: "hidden",
                }}
              >
                <img
                  src="https://icdn.dantri.com.vn/thumb_w/640/2021/02/27/diem-danh-7-guong-mat-hot-girl-xinh-dep-noi-bat-trong-thang-2-docx-1614441452345.jpeg"
                  alt="user avt"
                  width="100%"
                />
              </div>
              <Typography>useremail@gmail.com</Typography>
              <Typography>User Name</Typography>
            </div>

            <div className="pt-7">
              <div className=" text-center">
                <Typography className="font-weight-bold">
                  Các tài khoản liên kết
                </Typography>
                <p className="font-13">
                  Bạn có thể đăng nhập qua Google, Facebook nhanh vào website.
                  Để đăng nhập được bạn cần liên kết các tài khoản mạng xã hội
                  với tài khoản của website.
                </p>
              </div>

              <div className="flex-center flex-direction-column">
                <div>
                  <div className="d-flex flex-center">
                    <img
                      src="https://hoanghamobile.com/Content/web/img/login-facebook.png"
                      alt="fb"
                    />
                    <Typography
                      className="font-weight-bold font-20 ml-2"
                      style={{ color: "red" }}
                    >
                      Chưa liên kết
                    </Typography>
                  </div>
                  <Button
                    className="mt-4 p-3"
                    startIcon={<AccountCircleOutlinedIcon />}
                    style={{
                      background:
                        "linear-gradient(180deg,#00917a 0%,#00483d 100%)",
                      color: " #fff",
                      textTransform: "none",
                      borderRadius: "8px",
                    }}
                  >
                    Liên kết tài khoản Facebook
                  </Button>
                </div>

                <div className="mt-5">
                  <div className="d-flex flex-center">
                    <img
                      src="https://hoanghamobile.com/Content/web/img/login-google.png"
                      alt="fb"
                    />
                    <Typography
                      className="font-weight-bold font-20 ml-2"
                      style={{ color: "#333" }}
                    >
                      Chưa liên kết
                    </Typography>
                  </div>
                  <Button
                    className="mt-4 p-3"
                    startIcon={<AccountCircleOutlinedIcon />}
                    style={{
                      background:
                        "linear-gradient(180deg,#00917a 0%,#00483d 100%)",
                      color: " #fff",
                      textTransform: "none",
                      borderRadius: "8px",
                    }}
                  >
                    Liên kết tài khoản Google
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};

export default UserInfoScreen;
