import React from "react";

import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import { Container, Grid, Typography } from "@mui/material";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import cx from "classnames";
import { useNavigate } from "react-router-dom";

import styles from "./DashboardScreen.module.scss";
import HomeImg from "../../assets/images/icon-account-home.png";

const DashboardScreen = () => {
  const navigate = useNavigate();

  const columns: GridColDef[] = [
    { field: "sort", headerName: "#", width: 10, sortable: false },
    { field: "id", headerName: "Mã đơn hàng", width: 130, sortable: false },
    { field: "orderAt", headerName: "Ngày đặt", width: 90, sortable: false },
    { field: "cost", headerName: "Tổng tiền", width: 70, sortable: false },
    {
      field: "sale",
      headerName: "Giảm giá",
      width: 70,
      sortable: false,
    },
    {
      field: "item",
      headerName: "Sản phẩm đã đặt",
      sortable: false,
      width: 190,
    },
  ];

  const rows = [
    { sort: 1, id: 1, orderAt: "Snow", cost: "Jon", sale: 35, item: "test" },
  ];

  return (
    <Container className={cx(styles.root)}>
      <div className="mb-4">
        <Typography className="font-24 font-weight-bold mb-6">
          Bảng điều khiển
        </Typography>
        <div className={cx(styles.banner, "flex-align-center")}>
          <div>
            <Typography
              className="font-weight-bold font-20"
              style={{ color: "#47637D" }}
            >
              CHÀO MỪNG QUAY TRỞ LẠI
            </Typography>
            <Typography
              className="font-13 my-3"
              style={{ fontStyle: "italic" }}
            >
              Tổng quát các hoạt động của bạn tại đây
            </Typography>
          </div>
          <div>
            <img src={HomeImg} alt="banner" />
          </div>
        </div>
      </div>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Typography className="font-weight-bold">
            Thông tin cá nhân
          </Typography>
          <div className={cx(styles.infoWrapper, "mt-5")}>
            <button
              onClick={() => navigate("/user/info")}
              style={{
                color: "#ccc",
                position: "absolute",
                right: 10,
                backgroundColor: "transparent",
                border: "none",
              }}
            >
              <i className="bx bx-edit font-25" />
            </button>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Họ và tên:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Tài khoản:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Ngày tháng năm sinh:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Ngày tham gia:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Email:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Địa chỉ:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Số điện thoại:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Tên công ty:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Địa chỉ công ty:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
            <div className="d-flex mb-5">
              <Typography className="font-weight-bold mr-3 font-13">
                Mã số thuế:
              </Typography>
              <Typography
                className="font-13 font-weight-bold"
                style={{ color: "#c5c5d1", fontStyle: "italic" }}
              >
                user name
              </Typography>
            </div>
          </div>
        </Grid>

        <Grid item xs={6}>
          <Typography className="font-weight-bold">Đơn hàng đã đặt</Typography>
          <div className={cx(styles.infoWrapper, "mt-5  flex-align-center ")}>
            <DataGrid rows={rows} columns={columns} />
          </div>
        </Grid>
      </Grid>

      <Grid container className="mt-6">
        <Grid item xs={12}>
          <Typography className="font-weight-bold">
            Sản phẩm yêu thích
          </Typography>
          <div className={cx(styles.infoWrapper, "mt-5 ")}>
            <button
              onClick={() => navigate("/user/favorite")}
              style={{
                color: "#ccc",
                position: "absolute",
                right: 10,
                backgroundColor: "transparent",
                border: "none",
              }}
            >
              <i className="bx bx-edit font-25" />
            </button>
            Chưa có sản phẩm yêu thích
          </div>
        </Grid>
      </Grid>

      <Grid container className="mt-6">
        <Grid item xs={12}>
          <Typography className="font-weight-bold">Quản lí đánh giá</Typography>
          <div className={cx(styles.infoWrapper, "mt-5 ")}>
            <button
              onClick={() => navigate("/user/rate")}
              style={{
                color: "#ccc",
                position: "absolute",
                right: 10,
                backgroundColor: "transparent",
                border: "none",
              }}
            >
              <RemoveRedEyeOutlinedIcon />
            </button>
            Bạn chưa gửi đánh giá nào cả
          </div>
        </Grid>
      </Grid>

      <Grid container className="mt-6">
        <Grid item xs={12}>
          <Typography className="font-weight-bold">
            Quản lí bình luận
          </Typography>
          <div className={cx(styles.infoWrapper, "mt-5 ")}>
            <button
              onClick={() => navigate("/user/comment")}
              style={{
                color: "#ccc",
                position: "absolute",
                right: 10,
                backgroundColor: "transparent",
                border: "none",
              }}
            >
              <RemoveRedEyeOutlinedIcon />
            </button>
            Bạn chưa gửi bình luận nào cả.
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};

export default DashboardScreen;
