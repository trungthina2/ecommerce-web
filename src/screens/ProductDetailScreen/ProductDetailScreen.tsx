/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable import/no-unresolved */
/* eslint-disable jsx-a11y/label-has-associated-control */

import { useEffect, useState } from "react";

import AddShoppingCartOutlinedIcon from "@mui/icons-material/AddShoppingCartOutlined";
import ArrowLeftIcon from "@mui/icons-material/ArrowLeft";
import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import { Box, Button, Container, Grid, Stack, Typography } from "@mui/material";
import Collapse from "@mui/material/Collapse";
import cx from "classnames";
import { useNavigate, useParams } from "react-router-dom";
import Slider from "react-slick";
import * as yup from "yup";

import ModalOrder from "@app/components/ModalOrder/Modal";
import CommentSection from "@app/components/oganism/CommentSection/CommentSection";
import ComparedProduct from "@app/components/oganism/ComparedProduct/ComparedProduct";
import ProductPromotion from "@app/components/oganism/ProductPromotion/ProductPromotion";
import RatingSection from "@app/components/oganism/RatingSection/RatingSection";
import Sale from "@app/components/oganism/Sale/Sale";
import { CITIES } from "@app/constants/city.constants";
import { FAKE_PRODUCT } from "@app/constants/product-detail.constants";
import { formattedNumber } from "@app/helpers/ultils";

import styles from "./ProductDetailScreen.module.scss";

const phoneRegExp = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;
// const yupUserInfo = yup.object().shape({
//   username: yup.string().required(),
//   phonenumber: yup
//     .string()
//     .required()
//     .matches(phoneRegExp, "Phone number is not valid"),
//   email: yup.string().email().required(),
// });

const ProductDetailScreen = () => {
  const navigate = useNavigate();
  const [isCollapse, setIsCollapse] = useState(false);
  const [nav1, setNav1] = useState<any>();
  const [nav2, setNav2] = useState<any>();
  const [openModal, setOpenModal] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [selectedReceive, setSelectedReceive] = useState("1");
  const [listDistrict, setListDistrict] = useState<any>([]);
  const [listStore, setListStore] = useState<any>([]);
  const [user, setUser] = useState<any>({
    username: "",
    phonenumber: "",
    email: "",
  });
  const [userAddress, setUserAddress] = useState<any>({
    city: "",
    district: "",
    address: "",
    note: "",
  });
  const [storeOder, setStoreOrder] = useState<any>({
    city: "",
    store: "",
    note: "",
  });

  const [errors, setErrors] = useState<any>({});

  // product id to get product detail
  const { id } = useParams();

  // fake data
  const productData = FAKE_PRODUCT[Number(id) - 1];

  sessionStorage.setItem("item1Id", JSON.stringify(id));

  const yupUserInfo = yup.lazy(values => {
    if (selectedReceive === "1") {
      return yup.object().shape({
        username: yup.string().required(),
        phonenumber: yup
          .string()
          .required()
          .matches(phoneRegExp, "Phone number is not valid"),
        email: yup.string().email().required(),
        city: yup.string().required(),
        district: yup.string().required(),
        address: yup.string().required(),
        note: yup.string(),
      });
    }
    return yup.object().shape({
      username: yup.string().required(),
      phonenumber: yup
        .string()
        .required()
        .matches(phoneRegExp, "Phone number is not valid"),
      email: yup.string().email().required(),
      city: yup.string().required(),
      store: yup.string().required(),
      note: yup.string(),
    });
  });

  const handleIncrease = () => {
    setQuantity(quantity + 1);
  };

  const handleDecrease = () => {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  };

  const handleChangeQuantity = (event: any) => {
    const value = parseInt(event.target.value, 10);
    // eslint-disable-next-line no-restricted-globals
    if (!isNaN(value)) {
      setQuantity(value);
    }
  };

  const handleOpenModal = () => {
    setUser({});
    setUserAddress({});
    setStoreOrder({});
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleReceiveChange = (event: any) => {
    setUserAddress({
      city: "",
      district: "",
      address: "",
      note: "",
    });
    setStoreOrder({
      city: "",
      store: "",
      note: "",
    });
    setSelectedReceive(event.target.value);
  };

  const fakeImg = [
    "https://cdn.hoanghamobile.com/i/preview/Uploads/2022/06/07/macbook-pro-m2-silver-1.png",
    "https://cdn.hoanghamobile.com/i/preview/Uploads/2022/06/07/macbook-pro-m2-gray-2.png",
    "https://cdn.hoanghamobile.com/i/preview/Uploads/2022/06/07/macbook-pro-m2-gray-3.png",
    "https://cdn.hoanghamobile.com/i/preview/Uploads/2022/06/07/macbook-pro-m2-gray-6.png",
    "https://cdn.hoanghamobile.com/i/preview/Uploads/2022/06/07/macbook-pro-m2-silver-3.png",
    "https://cdn.hoanghamobile.com/i/preview/Uploads/2022/06/07/macbook-pro-m2-silver-1.png",
    "https://cdn.hoanghamobile.com/i/preview/Uploads/2022/06/07/macbook-pro-m2-gray-6.png",
  ];

  const handleChangeInfo = (e: any) => {
    const { name, value } = e.target;
    setUser((prevValues: any) => ({
      ...prevValues,
      [name]: value,
    }));
  };

  const handleChangeInfoReceive = (e: any) => {
    const { name, value } = e.target;
    if (selectedReceive === "1") {
      setUserAddress((prevValues: any) => ({
        ...prevValues,
        [name]: value,
      }));
    } else {
      setStoreOrder((prevValues: any) => ({
        ...prevValues,
        [name]: value,
      }));
    }
  };

  const handleSubmitOrder = async () => {
    try {
      const userRecieveInfomation = {
        ...user,
        quantity,
        type: selectedReceive,
        productId: id,
      };
      if (selectedReceive === "1") {
        Object.assign(userRecieveInfomation, userAddress);
      } else {
        Object.assign(userRecieveInfomation, storeOder);
      }
      await yupUserInfo.validate(userRecieveInfomation, {
        abortEarly: false,
      });
      setErrors({});

      await navigate("/order", {
        state: { userRecieveInfomation, productData },
      });
    } catch (error) {
      // eslint-disable-next-line no-console
      // eslint-disable-next-line @typescript-eslint/no-shadow
      const errors: any = {};
      if (error?.inner) {
        // eslint-disable-next-line @typescript-eslint/no-shadow
        error.inner.forEach((error: any) => {
          errors[error.path] = error.message;
        });
      }
      setErrors(errors);
    }
  };

  useEffect(() => {
    if (userAddress.city) {
      const list = CITIES.filter(item => item.name === userAddress.city)[0]
        .districts;
      setListDistrict(list);
    }
  }, [userAddress.city]);

  useEffect(() => {
    if (storeOder.city) {
      const list = CITIES.filter(item => item.name === storeOder.city)[0]
        .stores;
      setListStore(list);
    }
  }, [storeOder.city]);

  const handleAddCart = () => {
    let cartItems: any[] = [];
    const cartItemsJson = localStorage.getItem("cartItems");
    if (cartItemsJson) {
      cartItems = [...JSON.parse(cartItemsJson as string)];
    }
    // eslint-disable-next-line eqeqeq
    const indexItem = cartItems?.findIndex((item: any) => item.id == id);
    if (indexItem !== -1) {
      cartItems[indexItem].quantity += quantity;
    } else {
      cartItems.push({ ...productData, quantity });
    }
    localStorage.setItem("cartItems", JSON.stringify(cartItems));
  };

  return (
    <div className={cx(styles.root)}>
      <Container className="px-0 py-3" maxWidth="lg">
        <Grid container spacing={2}>
          <Grid item xs>
            <Typography className="font-20 font-weight-bold">
              {productData?.name}
            </Typography>
            <Grid
              container
              spacing={3}
              className={cx(styles.inforWrapper, "mt-4")}
            >
              {/* item images list */}
              <Grid
                item
                xs={6}
                className={cx(styles.infoImage, "flex-justify-center")}
              >
                <Box sx={{ width: "100%", height: 370 }}>
                  <Slider
                    autoplay
                    autoplaySpeed={10000}
                    nextArrow={<ArrowRightIcon />}
                    prevArrow={<ArrowLeftIcon />}
                    className={cx(styles.firstSlider)}
                    asNavFor={nav2}
                    ref={slider1 => setNav1(slider1)}
                  >
                    {fakeImg.map((item: string) => (
                      <div>
                        <img width="100%" src={item} alt="mac" />
                      </div>
                    ))}
                  </Slider>
                  <Slider
                    autoplay
                    className={cx(styles.secondSlider)}
                    asNavFor={nav1}
                    ref={slider2 => setNav2(slider2)}
                    nextArrow={<ArrowRightIcon />}
                    prevArrow={<ArrowLeftIcon />}
                    slidesToShow={5}
                    swipeToSlide
                    focusOnSelect
                    centerMode
                  >
                    {fakeImg.map((item: string) => (
                      <div>
                        <img width={65} src={item} alt="mac" />
                      </div>
                    ))}
                  </Slider>
                </Box>
              </Grid>

              {/*  item information */}
              <Grid item xs={6} className={styles.infoText}>
                <div
                  className="d-flex flex-align-center"
                  style={{ maxHeight: 33 }}
                >
                  <Typography
                    className="font-25 font-weight-bold mr-3"
                    style={{ color: "#fd475a" }}
                  >
                    {formattedNumber(productData?.price)}
                  </Typography>
                  <Typography className="font-16 " style={{ color: "#333333" }}>
                    | đã bao gồm 10% VAT
                  </Typography>
                </div>
                <Button
                  className={cx(styles.shipBtn, "font-12 my-2")}
                  startIcon={
                    <LocalShippingOutlinedIcon
                      style={{ color: "#fff" }}
                      className="font-32"
                    />
                  }
                >
                  Miễn phí vận chuyển toàn quốc
                </Button>
                <Sale />

                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Button
                      className={cx(
                        styles.buyBtn,
                        "flex-direction-column flex-center"
                      )}
                      onClick={
                        productData?.availible ? handleOpenModal : undefined
                      }
                    >
                      {productData?.availible ? (
                        <>
                          <span>MUA NGAY</span>
                          <p className="font-11 my-0">Giao tận nhà (COD)</p>
                          <p className="font-11 my-0">Hoặc nhận tại cửa hàng</p>
                        </>
                      ) : (
                        <span>LIÊN HỆ</span>
                      )}
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      className={cx(
                        styles.addCartBtn,
                        "flex-direction-column flex-center"
                      )}
                      onClick={handleAddCart}
                    >
                      <AddShoppingCartOutlinedIcon className="font-22" />
                      <p className="font-11 my-0">Thêm vào giỏ hàng</p>
                    </Button>
                  </Grid>
                </Grid>

                <ProductPromotion />
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        {/* spec section */}
        <Grid container className="mt-6">
          <Grid item xs>
            <Grid container spacing={2}>
              <Grid item xs={8}>
                <div
                  className={cx(
                    styles.infoContainer,
                    "py-4 px-3 flex-direction-column flex-justify-center"
                  )}
                >
                  <Collapse in={isCollapse} collapsedSize={800}>
                    <Typography className="font-32 font-weight-bold text-center">
                      {productData?.title}
                    </Typography>
                    <p>{productData?.content}</p>
                  </Collapse>
                  <Button
                    style={{ color: "#00483d" }}
                    className="font-16 font-weight-bold mt-2 py-0"
                    onClick={() => setIsCollapse(!isCollapse)}
                  >
                    {isCollapse ? "THU GỌN" : "XEM THỂM"}
                  </Button>
                </div>
              </Grid>
              <Grid item xs={4}>
                <div
                  className="py-5 px-4"
                  style={{
                    backgroundColor: "#fff",
                    borderRadius: 14,
                    boxShadow: "0 3px 6px #00000029",
                  }}
                >
                  <Typography className="font-16 font-weight-bold">
                    {`Thông số kỹ thuật ${productData?.name}`}
                  </Typography>
                  <img src={productData.image} alt="spec" width="100%" />

                  <Stack className="font-14">
                    <div>
                      <span className="font-weight-bold">RAM: </span>
                      <span>{productData?.ram}</span>
                    </div>
                    <div>
                      <span className="font-weight-bold">
                        Khả năng nâng cấp:
                      </span>
                      <span> Không hỗ trợ</span>
                    </div>
                    <div>
                      <span className="font-weight-bold">Độ phân giải: </span>
                      <span>{productData?.resolution}</span>
                    </div>
                    <div>
                      <span className="font-weight-bold">
                        Kết nối không giây:
                      </span>
                      <span> Wi-Fi 6E (802.11ax), Bluetooth 5.3</span>
                    </div>
                    <div>
                      <span className="font-weight-bold">Kích thước:</span>
                      <span>{productData?.weight}</span>
                    </div>
                  </Stack>
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {/* compared products */}
        <ComparedProduct data={FAKE_PRODUCT} />

        {/* comments section */}
        <CommentSection />

        {/* Rate section */}
        <RatingSection />
        <ModalOrder open={openModal} onClose={handleCloseModal}>
          <div className={cx(styles.modal_content)}>
            <button
              className={cx(styles.btn_closs_modal)}
              onClick={handleCloseModal}
            >
              <div className={cx(styles.cross)} />
            </button>
            <div className={cx(styles.left)}>
              <div>
                <img src={fakeImg[4]} alt="" />
              </div>
              <div className={cx(styles.info)}>
                <p className={cx(styles.title)}>
                  MacBook Pro M2 13&quot; 2022 - 256GB - Chính hãng Apple Việt
                  Nam <br />
                  <span className={cx(styles.text_gray)}>
                    256GB - Silver (<label>SKU:</label>{" "}
                    <strong id="dfSKU">MNEP3SA</strong>)
                  </span>
                </p>
                <p className={cx(styles.price)}>
                  <strong id="quickOrderPrice" data-value="29990000">
                    29,990,000 ₫
                  </strong>
                  <small id="quickOrderPriceLast" data-value="31990000">
                    31,990,000 ₫
                  </small>
                </p>
              </div>
            </div>
            <div className={cx(styles.right)}>
              <h3>Đặt hàng sản phẩm</h3>
              <label>Số lượng</label>
              <div className={cx(styles.modal_quantity)}>
                <button
                  onClick={handleDecrease}
                  className={cx(styles.btn_decrease)}
                >
                  -
                </button>
                <input
                  type="number"
                  value={quantity}
                  onChange={handleChangeQuantity}
                  className={cx(styles.input_quantity)}
                />
                <button
                  onClick={handleIncrease}
                  className={cx(styles.btn_increase)}
                >
                  +
                </button>
              </div>
              <Grid container spacing={2} className={cx(styles.info_user)}>
                <Grid item xs={12}>
                  <label>Họ tên</label>
                  <input
                    type="text"
                    name="username"
                    placeholder="Họ và tên *"
                    onChange={handleChangeInfo}
                  />
                  {errors?.username && (
                    <p className={cx(styles.error_text)}>{errors?.username}</p>
                  )}
                </Grid>
                <Grid item xs={6}>
                  <label>Điện thoại</label>
                  <input
                    type="number"
                    name="phonenumber"
                    placeholder="Số điện thoại *"
                    onChange={handleChangeInfo}
                  />
                  {errors?.phonenumber && (
                    <p className={cx(styles.error_text)}>
                      {errors?.phonenumber}
                    </p>
                  )}
                </Grid>
                <Grid item xs={6}>
                  <label>Email</label>
                  <input
                    type="email"
                    name="email"
                    placeholder="Email *"
                    onChange={handleChangeInfo}
                  />
                  {errors?.email && (
                    <p className={cx(styles.error_text)}>{errors?.email}</p>
                  )}
                </Grid>
              </Grid>
              <Grid container spacing={2} className={cx(styles.receive)}>
                <Grid item xs={12}>
                  <label> Hình thức nhận hàng</label>
                </Grid>
                <Grid item xs={6}>
                  <div
                    id="payType_1"
                    className={cx(
                      styles.payment_opt,
                      selectedReceive === "1" ? styles.checked : null
                    )}
                  >
                    <label className={cx(styles.radio_ctn)} htmlFor="radio1">
                      <div className={cx(styles.radiobox)}>
                        <input
                          type="radio"
                          id="radio1"
                          name="myRadio"
                          value="1"
                          checked={selectedReceive === "1"}
                          onChange={handleReceiveChange}
                        />
                        <label htmlFor="radio1" />
                      </div>
                      <span>Nhận hàng tại nhà</span>
                    </label>
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div
                    id="payType_1"
                    className={cx(
                      styles.payment_opt,
                      selectedReceive === "2" ? styles.checked : null
                    )}
                  >
                    <label className={cx(styles.radio_ctn)} htmlFor="radio2">
                      <div className={cx(styles.radiobox)}>
                        <input
                          type="radio"
                          id="radio2"
                          name="myRadio"
                          value="2"
                          checked={selectedReceive === "2"}
                          onChange={handleReceiveChange}
                        />
                        <label htmlFor="radio2" />
                      </div>
                      <span>Nhận hàng tại cửa hàng</span>
                    </label>
                  </div>
                </Grid>
              </Grid>
              <Grid
                container
                spacing={1}
                className={cx(styles.delivery_address)}
              >
                <Grid item xs={12}>
                  <label>
                    {selectedReceive === "1" ? "Địa chỉ" : "Nơi nhận hàng"}
                  </label>
                </Grid>
                {selectedReceive === "1" ? (
                  <>
                    <Grid item xs={6}>
                      <select
                        name="city"
                        id="city"
                        onChange={handleChangeInfoReceive}
                      >
                        <option value="">Tỉnh/Thành phố *</option>

                        {CITIES.map(item => {
                          const { name } = item;
                          return (
                            <option key={name} value={name}>
                              {name}
                            </option>
                          );
                        })}
                      </select>
                      {errors?.city && (
                        <p className={cx(styles.error_text)}>{errors?.city}</p>
                      )}
                    </Grid>
                    <Grid item xs={6}>
                      <select
                        name="district"
                        id="district"
                        onChange={handleChangeInfoReceive}
                      >
                        <option value="">Quận/Huyện *</option>

                        {listDistrict.map((item: any) => (
                          <option key={item} value={item}>
                            {item}
                          </option>
                        ))}
                      </select>
                      {errors?.district && (
                        <p className={cx(styles.error_text)}>
                          {errors?.district}
                        </p>
                      )}
                    </Grid>
                    <Grid item xs={12}>
                      <input
                        type="text"
                        name="address"
                        placeholder="Địa chỉ nhận hàng *"
                        onChange={handleChangeInfoReceive}
                      />
                      {errors?.address && (
                        <p className={cx(styles.error_text)}>
                          {errors?.address}
                        </p>
                      )}
                    </Grid>
                    <Grid item xs={12}>
                      <textarea
                        name="note"
                        placeholder="Ghi chú"
                        onChange={handleChangeInfoReceive}
                      />
                      {errors?.note && (
                        <p className={cx(styles.error_text)}>{errors?.note}</p>
                      )}
                    </Grid>
                  </>
                ) : (
                  <>
                    <Grid item xs={6}>
                      <select
                        name="city"
                        id="city"
                        onChange={handleChangeInfoReceive}
                      >
                        <option value="">Tỉnh/Thành phố *</option>
                        {CITIES.map(item => {
                          const { name } = item;
                          return (
                            <option key={name} value={name}>
                              {name}
                            </option>
                          );
                        })}
                      </select>
                      {errors?.city && (
                        <p className={cx(styles.error_text)}>{errors?.city}</p>
                      )}
                    </Grid>
                    <Grid item xs={6}>
                      <select
                        name="store"
                        id="district"
                        onChange={handleChangeInfoReceive}
                      >
                        <option value="">Cửa hàng *</option>
                        {listStore.map((item: any) => (
                          <option key={item} value={item}>
                            {item}
                          </option>
                        ))}
                      </select>
                      {errors?.store && (
                        <p className={cx(styles.error_text)}>{errors?.store}</p>
                      )}
                    </Grid>
                    <Grid item xs={12}>
                      <textarea
                        name="note"
                        placeholder="Ghi chú"
                        onChange={handleChangeInfoReceive}
                      />
                      {errors?.note && (
                        <p className={cx(styles.error_text)}>{errors?.note}</p>
                      )}
                    </Grid>
                  </>
                )}
              </Grid>
              <Grid container>
                <Grid
                  item
                  xs={12}
                  className={cx(styles.btn_order, "flex-center")}
                  onClick={handleSubmitOrder}
                >
                  <button>TIẾN HÀNH ĐẶT HÀNG</button>
                </Grid>
              </Grid>
            </div>
          </div>
        </ModalOrder>
      </Container>
    </div>
  );
};

export default ProductDetailScreen;
