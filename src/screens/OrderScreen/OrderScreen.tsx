import React from "react";

import { Container, Typography } from "@mui/material";
import cx from "classnames";

import styles from "./OrderScreen.module.scss";
import OrderImg from "../../assets/images/icon-account-order.png";

const OrderScreen = () => {
  return (
    <Container className={cx(styles.root)}>
      <div className="mb-4">
        <Typography className="font-24 font-weight-bold mb-6">
          Bảng điều khiển
        </Typography>
        <div className={cx(styles.banner, "flex-align-center")}>
          <div>
            <Typography
              className="font-weight-bold font-20"
              style={{ color: "#47637D" }}
            >
              CHÀO MỪNG QUAY TRỞ LẠI
            </Typography>
            <Typography
              className="font-13 my-3"
              style={{ fontStyle: "italic" }}
            >
              Kiểm tra thông tin đơn hàng của bạn tại đây
            </Typography>
          </div>
          <div>
            <img src={OrderImg} alt="banner" />
          </div>
        </div>
      </div>

      <Typography className="font-weight-bold">Đơn hàng đã đặt</Typography>
    </Container>
  );
};

export default OrderScreen;
