import * as yup from "yup";

const phoneRegExp =
  /^(?:\+84|0)(?:3[2-9]|5[2689]|7[0|6-9]|8[1-689]|9[0-46-9])\d{7}$/;

export const SignupSchema = yup.object().shape({
  username: yup.string().required("Vui lòng nhập tên tài khoản"),
  fullname: yup.string().required("Vui lòng nhập họ và tên"),
  password: yup
    .string()
    .required("Vui lòng nhập mật khẩu")
    .min(6, "Mật khẩu phải có tối thiểu 6 kí tự"),
  password2: yup
    .string()
    .required(" Vui lòng xác nhận mật khẩu")
    .test("match", "Xác nhận mật khẩu không trung khớp", function (value) {
      return value === this.resolve(yup.ref("password"));
    }),
  email: yup
    .string()
    .email("Vui lòng nhập đúng định dạng e-mail")
    .required("Vui lòng nhập em-mail"),
  birthday: yup.string(),
  phone: yup
    .string()
    .required("Vui lòng nhập số điện thoại")
    .matches(phoneRegExp, "Số điện thoại không đúng định dạng"),
  address: yup.string().required("Vui lòng nhập địa chỉ"),
  city: yup.string(),
  province: yup.string(),
});

export const LoginSchema = yup.object().shape({
  username: yup.string().required("Vui lòng nhập tài khoản"),
  password: yup
    .string()
    .required("Vui lòng nhập mật khẩu")
    .min(6, "Mật khẩu phải có tối thiểu 6 kí tự"),
  remember_me: yup.string(),
});

export const ForgotPassSchema = yup.object().shape({
  email: yup
    .string()
    .email("Vui lòng nhập đúng định dạng e-mail")
    .required("Vui lòng nhập e-mail"),
});

export const UpdateUserInfoSchema = yup.object().shape({
  fullname: yup.string().required("Vui lòng nhập họ và tên"),
  password: yup.string(),
  password2: yup
    .string()
    .test("match", "Xác nhận mật khẩu không trung khớp", function (value) {
      return value === this.resolve(yup.ref("password"));
    }),
  email: yup
    .string()
    .email("Vui lòng nhập đúng định dạng e-mail")
    .required("Vui lòng nhập em-mail"),
  birthday: yup.string(),
  phone: yup
    .string()
    .matches(phoneRegExp, "Số điện thoại không đúng định dạng")
    .required("Vui lòng nhập số điện thoại"),
  address: yup.string().required("Vui lòng nhập địa chỉ"),
  city: yup.string(),
  province: yup.string(),
  company: yup.string(),
  company_address: yup.string(),
  company_taxcode: yup.string(),
});

export const CartInfomationSchema = yup.object().shape({
  fullname: yup.string().required("Vui lòng nhập họ và tên"),
  email: yup
    .string()
    .email("Vui lòng nhập đúng định dạng e-mail")
    .required("Vui lòng nhập em-mail"),
  phone: yup
    .string()
    .typeError("Vui lòng nhập đúng định dạng số điện thoại")
    .required("Vui lòng nhập số điện thoại"),
});

export const UserInfoSchema = (selectedReceive: string) => {
  return yup.lazy((values: any) => {
    if (selectedReceive === "1") {
      return yup.object().shape({
        fullname: yup.string().required(),
        phone: yup
          .string()
          .required()
          .matches(phoneRegExp, "Phone number is not valid"),
        email: yup.string().email().required(),
        city: yup.string().required(),
        district: yup.string().required(),
        address: yup.string().required(),
        note: yup.string(),
      });
    }
    return yup.object().shape({
      fullname: yup.string().required(),
      phone: yup
        .string()
        .required()
        .matches(phoneRegExp, "Phone number is not valid"),
      email: yup.string().email().required(),
      city: yup.string().required(),
      store: yup.string().required(),
      note: yup.string(),
    });
  });
};
