export const formattedNumber = (number: number) =>
  number.toLocaleString("vi-VN", {
    style: "currency",
    currency: "VND",
  });
