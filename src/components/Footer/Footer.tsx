import React from "react";

import { Chip, Container, Grid, Typography } from "@mui/material";
import cx from "classnames";
import { useLocation } from "react-router-dom";

import { CONTACT_ITEM, SERVICES_ITEM } from "@app/constants/footer.constants";

import styles from "./Footer.module.scss";

const Footer = () => {
  const location = useLocation();

  const isInUserPage = location.pathname.includes("/user");

  return (
    <div className={isInUserPage ? "d-none" : ""}>
      <Container
        style={{ display: "flex" }}
        maxWidth="lg"
        className={cx(
          styles.footer,
          "flex-align-center flex-justify-space-around flex-direction-column"
        )}
      >
        <Grid container spacing={1} className={cx(styles.contentWrapper)}>
          <Grid item xs={2}>
            <Typography className="font-15 font-weight-bold">
              Hỗ trợ - Dịch vụ
            </Typography>
            <ul>
              {SERVICES_ITEM.map((item: string) => (
                <li className="font-weight-regular">{item}</li>
              ))}
            </ul>
          </Grid>

          <Grid item xs={2}>
            <Typography className="font-15 font-weight-bold">
              Thông tin liên hệ
            </Typography>
            <ul>
              {CONTACT_ITEM.map((item: string) => (
                <li className="font-weight-regular">{item}</li>
              ))}
            </ul>
          </Grid>

          <Grid item xs={2}>
            <Typography className="font-15 font-weight-bold">
              Hệ thống 127 siêu thị trên toàn quốc
            </Typography>
            <p className="font-13 font-weight-regular mt-0">
              Danh sách 127 siêu thị trên toàn quốc
            </p>
          </Grid>

          <Grid item xs={2}>
            <Typography className="font-15 font-weight-bold">
              Tổng đài
            </Typography>
            <Chip
              className="font-18 font-weight-bold mt-1"
              label="1900.2091"
              style={{
                backgroundColor: "#fff",
                color: "#00483d",
                borderRadius: 4,
              }}
            />
          </Grid>
          <Grid item xs={2}>
            <Typography className="font-15 font-weight-bold">
              Thanh toán miến phí
            </Typography>

            <Grid container className="mb-2 mt-1">
              <Grid item xs={6}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/logo-visa.png"
                  alt="visa"
                />
              </Grid>
              <Grid item xs={6}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/logo-master.png"
                  alt="visa"
                />
              </Grid>
            </Grid>
            <Grid container className="mb-2">
              <Grid item xs={6}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/logo-jcb.png"
                  alt="visa"
                />
              </Grid>
              <Grid item xs={6}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/logo-samsungpay.png"
                  alt="visa"
                />
              </Grid>
            </Grid>
            <Grid container className="mb-2">
              <Grid item xs={6}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/logo-atm.png"
                  alt="visa"
                />
              </Grid>
              <Grid item xs={6}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/logo-vnpay.png"
                  alt="visa"
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={2}>
            <Typography className="font-15 font-weight-bold">
              Hình thức vận chuyển
            </Typography>
            <Grid container className="mb-2 mt-1">
              <Grid item xs={6}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/nhattin.jpg"
                  alt="visa"
                />
              </Grid>
              <Grid item xs={6}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/vnpost.jpg"
                  alt="visa"
                />
              </Grid>
            </Grid>
            <Grid container className="mb-2 mt-1">
              <Grid item xs={12}>
                <img
                  src="https://hoanghamobile.com/Content/web/img/logo-bct.png"
                  alt="visa"
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <div className="text-center font-12 mt-2">
          <p className="my-1">
            © 2020. CÔNG TY CỔ PHẦN XÂY DỰNG VÀ ĐẦU TƯ THƯƠNG MẠI HOÀNG HÀ. MST:
            0106713191. (Đăng ký lần đầu: Ngày 15 tháng 12 năm 2014, Đăng ký
            thay đổi ngày 24/11/2022)
          </p>
          <p className="my-1">
            GP số 426/GP-TTĐT do sở TTTT Hà Nội cấp ngày 22/01/2021
          </p>
          <p className="my-1">
            Địa chỉ: Số 89 Đường Tam Trinh, Phường Mai Động, Quận Hoàng Mai,
            Thành Phố Hà Nội, Việt Nam. Điện thoại: 1900.2091. Chịu trách nhiệm
            nội dung: Hoàng Ngọc Chiến.
          </p>
        </div>
      </Container>
    </div>
  );
};

export default Footer;
