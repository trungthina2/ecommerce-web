import React from "react";

import Modal from "@mui/material/Modal";
import cx from "classnames";

import styles from "./ModalOrder.module.scss";

const ModalOrder = ({ open, onClose, children }: any) => {
  return (
    <Modal open={open} onClose={onClose}>
      <div className={cx(styles.modal_container)}>{children}</div>
    </Modal>
  );
};

export default ModalOrder;
