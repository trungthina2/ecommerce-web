import cx from "classnames";

import styles from "./BannerListComponent.module.scss";

const BannerListComponent = (props: any) => {
  const { data } = props;
  return (
    <div
      className={cx(styles.banner)}
      style={{
        backgroundColor: data.backgroundColor,
        color: data.color,
      }}
    >
      <div
        className={cx(styles.banner_text_container)}
        style={{
          borderTop: `29px solid ${data.colorMain}`,
        }}
      >
        <p className={cx(styles.banner_text)}>{data.text}</p>
      </div>
    </div>
  );
};

export default BannerListComponent;
