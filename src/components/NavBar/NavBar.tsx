/* eslint-disable react/no-array-index-key */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";

import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import ChatOutlinedIcon from "@mui/icons-material/ChatOutlined";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import CloseIcon from "@mui/icons-material/Close";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import FlashOnOutlinedIcon from "@mui/icons-material/FlashOnOutlined";
import HandymanOutlinedIcon from "@mui/icons-material/HandymanOutlined";
import HeadphonesIcon from "@mui/icons-material/Headphones";
import HelpIcon from "@mui/icons-material/Help";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import InstallMobileOutlinedIcon from "@mui/icons-material/InstallMobileOutlined";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import LaptopIcon from "@mui/icons-material/Laptop";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import LogoutOutlinedIcon from "@mui/icons-material/LogoutOutlined";
import MenuIcon from "@mui/icons-material/Menu";
import MonitorIcon from "@mui/icons-material/Monitor";
import MoodIcon from "@mui/icons-material/Mood";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import NewspaperOutlinedIcon from "@mui/icons-material/NewspaperOutlined";
import PersonIcon from "@mui/icons-material/Person";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import PhoneAndroidIcon from "@mui/icons-material/PhoneAndroid";
import PhoneIphoneIcon from "@mui/icons-material/PhoneIphone";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import QuestionAnswerIcon from "@mui/icons-material/QuestionAnswer";
import RateReviewOutlinedIcon from "@mui/icons-material/RateReviewOutlined";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import SimCardOutlinedIcon from "@mui/icons-material/SimCardOutlined";
import TabletAndroidIcon from "@mui/icons-material/TabletAndroid";
import TuneOutlinedIcon from "@mui/icons-material/TuneOutlined";
import TvIcon from "@mui/icons-material/Tv";
import UnarchiveOutlinedIcon from "@mui/icons-material/UnarchiveOutlined";
import UsbOutlinedIcon from "@mui/icons-material/UsbOutlined";
import VideogameAssetOutlinedIcon from "@mui/icons-material/VideogameAssetOutlined";
import WatchIcon from "@mui/icons-material/Watch";
import {
  Badge,
  Button,
  Chip,
  Grid,
  IconButton,
  Stack,
  Typography,
} from "@mui/material";
import Container from "@mui/material/Container";
import cx from "classnames";
import { useForm } from "react-hook-form";
import { Link, NavLink, useLocation, useNavigate } from "react-router-dom";

import firebaseInstance from "@app/config/firebase";
import { HEADER_MENU } from "@app/constants/navbar.constants";
import { FAKE_PRODUCT } from "@app/constants/product-detail.constants";
import { formattedNumber } from "@app/helpers/ultils";
import { setIsLogedIn, setIsNotLogedIn } from "@app/redux/example.slice";
import store, { useAppDispatch, useAppSelector } from "@app/redux/store";

import styles from "./NavBar.module.scss";

const NavBar = ({ children }: any) => {
  const [searchData, setSearchData] = useState<any[]>([]);
  const { register, handleSubmit } = useForm();
  const [isSticky, setIsSticky] = useState(false);
  const [isOpenChat, setIsOpenChat] = useState<boolean>(false);
  const [isOpenSideMenu, setIsOpenSideMenu] = useState<boolean>(false);
  const [isOpenSideSubNav, setIsOpenSideSubNav] = useState<boolean>(false);
  const [currentItem, setCurrentItem] = useState<number>();
  const location = useLocation();

  const isInUserPage = location.pathname.includes("/user");
  const isLogIn = useAppSelector(state => state?.auth.isLogedIn);
  const userEmail = localStorage.getItem("userEmail");
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  if (location.pathname !== "/laptop") {
    sessionStorage.removeItem("cpu");
    sessionStorage.removeItem("brand");
    sessionStorage.removeItem("ram");
    sessionStorage.removeItem("prices");
    sessionStorage.removeItem("category");
    sessionStorage.removeItem("resolutions");
    sessionStorage.removeItem("screenSize");
    sessionStorage.removeItem("graphicCard");
    sessionStorage.removeItem("hardDrives");
    sessionStorage.removeItem("sorts");
  }

  const PRODUCT_MENU_ITEM = [
    { id: 1, title: "ĐIỆN THOẠI", icon: <PhoneAndroidIcon /> },
    { id: 2, title: " ĐỒNG HỒ", icon: <WatchIcon /> },
    { id: 3, title: " LAPTOP", icon: <LaptopIcon /> },
    { id: 4, title: " APPLE", icon: <PhoneIphoneIcon /> },
    { id: 5, title: " MÀN HÌNH", icon: <MonitorIcon /> },
    { id: 6, title: " SMART TV", icon: <TvIcon /> },
    { id: 7, title: " TABLET", icon: <TabletAndroidIcon /> },
    { id: 8, title: " ÂM THANH", icon: <HeadphonesIcon /> },
    { id: 9, title: " SMART HOME", icon: <HomeOutlinedIcon /> },
    { id: 10, title: " PHỤ KIỆN", icon: <UsbOutlinedIcon /> },
    { id: 11, title: " ĐỒ CHƠI CN", icon: <VideogameAssetOutlinedIcon /> },
    { id: 12, title: " MÁY TRÔI", icon: <InstallMobileOutlinedIcon /> },
    { id: 13, title: " SỬA CHỮA", icon: <HandymanOutlinedIcon /> },
    { id: 14, title: " SIM THẺ", icon: <SimCardOutlinedIcon /> },
    { id: 15, title: " TIN HOT ", icon: <NewspaperOutlinedIcon /> },
    { id: 16, title: " ƯU ĐÃI", icon: <FlashOnOutlinedIcon /> },
  ];

  const BRAND_ITEM_1 = ["Apple", "Acer", "Huawei", "HP", "Xiaomi"];

  const BRAND_ITEM_2 = ["ASUS", "MSI", "Surface", "GIGABYTE", "Mastel"];

  const BRAND_ITEM_3 = ["Dell", "LG", "Lenovo", "Itel"];

  const PRICE_ITEM = [
    { title: "12 đến 15 triệu", from: 12000000, to: 15000000 },
    { title: "15 đến 20 triệu", from: 15000000, to: 20000000 },
    { title: "Trên 100 triệu", from: 100000000, to: 0 },
  ];

  useEffect(() => {
    if (localStorage.getItem("userId")) {
      store.dispatch(setIsLogedIn());
    }
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;

      if (scrollPosition > 140) {
        setIsSticky(true);
      } else {
        setIsSticky(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [isSticky]);

  const handleSearch = (value: string) => {
    if (value === "") {
      setSearchData([]);
      return;
    }
    setSearchData(
      FAKE_PRODUCT.filter((item: any) => {
        const lowerCaseItem = item.name.toLowerCase().trim();
        const lowerCaseSearchText = value.toLowerCase().trim();

        return lowerCaseItem.includes(lowerCaseSearchText);
      })
    );
  };

  const handleLogout = () => {
    firebaseInstance.signOut().then(() => dispatch(setIsNotLogedIn()));
  };

  const totalCartItem = () => {
    let total = 0;
    const cartItemsJson = localStorage.getItem("cartItems");
    if (cartItemsJson) {
      let cartItems: any[] = [];
      cartItems = [...JSON.parse(cartItemsJson as string)];
      cartItems.forEach((item: any) => {
        total += parseInt(item.quantity, 10);
      });
    }
    return total;
  };

  return (
    <>
      <div className={cx(styles.SmallScreenHeader, "px-1")}>
        <div className={isOpenSideMenu ? cx(styles.SideMenu) : "d-none"}>
          <button
            className={cx(styles.closeBtn)}
            onClick={() => {
              setIsOpenSideMenu(false);
              setIsOpenSideSubNav(false);
            }}
          >
            <CloseIcon className="font-40" />
          </button>
          <div className={cx(styles.user, "flex-align-center")}>
            <div className={cx(styles.avatar, "flex-center")}>
              <PersonIcon className="font-32" />
            </div>
            <div className="ml-2">
              <a className="font-15" href="/login">
                Đăng nhập
              </a>
              <p className="font-13">Đăng nhập để nhận nhiều ưu đãi</p>
            </div>
          </div>

          <ul className={styles.productSideMenu}>
            {PRODUCT_MENU_ITEM.map((item, index) => (
              <>
                <li
                  key={item.id}
                  className="flex-space-between flex-align-center px-3"
                >
                  <Link to="/laptop" onClick={() => setIsOpenSideMenu(false)}>
                    <span className=" flex-center font-15">
                      {item.icon}
                      <span className="font-weight-bold font-15 ml-2">
                        {item.title}
                      </span>
                    </span>
                  </Link>
                  <span>
                    <PlayArrowIcon
                      onClick={() => {
                        setCurrentItem(item.id);
                        setIsOpenSideSubNav(!isOpenSideSubNav);
                      }}
                    />
                  </span>
                </li>
                <div
                  className={
                    isOpenSideSubNav && currentItem === index + 1
                      ? cx(styles.productSideSubNav)
                      : "d-none"
                  }
                >
                  <Grid container spacing={2}>
                    <Grid item xs={24} className="d-flex">
                      <Grid xs={10} className="pl-3">
                        <Typography className="font-weight-bold font-13 py-1">
                          HÃNG SẢN XUẤT
                        </Typography>
                        <div className="d-flex">
                          <Grid xs={4}>
                            <ul>
                              {BRAND_ITEM_1.map((items: string) => (
                                <li>
                                  <Link
                                    to="/laptop"
                                    onClick={() =>
                                      sessionStorage.setItem("brand", items)
                                    }
                                  >
                                    {items}
                                  </Link>
                                </li>
                              ))}
                            </ul>
                          </Grid>
                          <Grid xs={4}>
                            <ul>
                              {BRAND_ITEM_2.map(items => (
                                <li>
                                  <Link
                                    to="/laptop"
                                    onClick={() =>
                                      sessionStorage.setItem("brand", items)
                                    }
                                  >
                                    {items}
                                  </Link>
                                </li>
                              ))}
                            </ul>
                          </Grid>
                          <Grid xs={4}>
                            <ul>
                              {BRAND_ITEM_3.map(items => (
                                <li>
                                  <Link
                                    to="/laptop"
                                    onClick={() =>
                                      sessionStorage.setItem("brand", items)
                                    }
                                  >
                                    {items}
                                  </Link>
                                </li>
                              ))}
                            </ul>
                          </Grid>
                        </div>
                      </Grid>
                      <Grid xs={4} className="pl-6">
                        <Typography className="font-weight-bold font-13 py-1">
                          MỨC GIÁ
                        </Typography>
                        <ul>
                          {PRICE_ITEM.map(items => (
                            <li>
                              <Link
                                to="/laptop"
                                onClick={() =>
                                  sessionStorage.setItem(
                                    "prices",
                                    JSON.stringify(items)
                                  )
                                }
                              >
                                {items.title}
                              </Link>
                            </li>
                          ))}
                        </ul>
                      </Grid>
                      <Grid xs={6}>
                        <Typography className="font-weight-bold font-13 py-1">
                          PHÂN LOẠI SẢN PHẨM
                        </Typography>
                        <ul>
                          <li>
                            <Link to="/laptop">Cao cấp - Sang trọng</Link>
                          </li>
                          <li>
                            <Link to="/laptop">Đồ họa kĩ thuật</Link>
                          </li>
                          <li>
                            <Link to="/laptop">Học tập văn phòng</Link>
                          </li>
                          <li>
                            <Link to="/laptop">Laptop Gaming</Link>
                          </li>
                          <li>
                            <Link to="/laptop">Mỏng nhẹ</Link>
                          </li>
                        </ul>
                      </Grid>
                    </Grid>
                  </Grid>
                </div>
              </>
            ))}
          </ul>

          <ul className={styles.serviceSideMenu}>
            <li>
              <a href="/introduction">
                <HelpIcon /> Giới thiệu
              </a>
            </li>
            <li>
              <a href="/viewed">
                <RemoveRedEyeIcon /> Sản phẩm đã xem
              </a>
            </li>
            <li>
              <a href="/service">
                <CheckCircleOutlineIcon /> Trung tâm bảo hành
              </a>
            </li>
            <li>
              <a href="/order">
                <LocalShippingOutlinedIcon /> Tra cứu đơn hàng
              </a>
            </li>
          </ul>
        </div>
        <Grid container className="flex-center">
          <Grid item xs={3}>
            <button onClick={() => setIsOpenSideMenu(!isOpenSideMenu)}>
              <MenuIcon className="font-24" />
            </button>
          </Grid>
          <Grid item className={cx(styles.Logo, "flex-justify-center")} xs>
            <Link to="/">
              <img
                src="https://hoanghamobile.com/Content/web/img/logo-text.png"
                alt="logo"
                width="256px"
              />
            </Link>
          </Grid>
          <Grid item xs={3} className="flex-justify-end p-2">
            <Link to="/cart">
              <IconButton>
                <Badge
                  badgeContent={totalCartItem()}
                  color="success"
                  className={cx(styles.badge)}
                >
                  <ShoppingCartOutlinedIcon className="font-40" />
                </Badge>
              </IconButton>
            </Link>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={24} style={{ position: "relative" }}>
            <form
              onSubmit={handleSubmit((data: any) => {
                console.log(data);
              })}
            >
              <input
                className={cx(styles.searchBox, "font-14")}
                placeholder="Hôm nay bạn cần tìm gì?"
                {...register("searchkey")}
                onChange={e => handleSearch(e.target.value)}
              />
              <button className={cx(styles.searhBtn)} type="submit">
                <SearchIcon />
              </button>
            </form>
            {searchData.length > 0 && (
              <div className={cx(styles.searchResult)}>
                <ul>
                  {searchData.map((item: any) => (
                    <Link
                      to={"/product/:id".replace(":id", item?.id)}
                      onClick={() => {
                        setSearchData([]);
                      }}
                    >
                      <li>
                        <Grid container style={{ maxHeight: "70px" }}>
                          <Grid item xs={2}>
                            <img src={item.imgUrl} alt="tét" width="70%" />
                          </Grid>
                          <Grid
                            item
                            xs={10}
                            className="flex-direction-column flex-space-between d-flex"
                          >
                            <Grid item xs={12}>
                              <Typography
                                className="font-weight-bold"
                                style={{ color: "#000" }}
                              >
                                {item.name}
                              </Typography>
                            </Grid>
                            <Grid item xs={12}>
                              <Typography
                                className="font-15 font-weight-bold"
                                style={{ color: "#fd475a" }}
                              >
                                {formattedNumber(item.price)}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Grid>
                      </li>
                    </Link>
                  ))}
                </ul>
              </div>
            )}
          </Grid>
        </Grid>
      </div>
      <div className={cx(styles.LargeScreenHeder)}>
        <div className={cx(styles.root, "font-13")}>
          <Container className={cx(styles.Header)} maxWidth="lg">
            <Grid container spacing={2}>
              {HEADER_MENU.map(item => (
                <Grid item>
                  <Link to={item.path}>{item.label}</Link>
                </Grid>
              ))}
              <Grid item>
                {isLogIn ? (
                  <span className={cx(styles.userName, "mr-2")}>
                    <Link to="/reports">{userEmail}</Link>
                    <div className={styles.menuDropDown}>
                      <ul>
                        <li>
                          <NavLink className="flex-align-center" to="/user">
                            <TuneOutlinedIcon className="font-15 mr-6" />
                            <Typography className="font-12">
                              Bảng điều khiển
                            </Typography>
                          </NavLink>
                        </li>
                        <li>
                          <NavLink
                            className="flex-align-center"
                            to="/user/info"
                          >
                            <AccountCircleOutlinedIcon className="font-15 mr-6" />
                            <Typography className="font-12">
                              Thông tin tài khoản
                            </Typography>
                          </NavLink>
                        </li>
                        <li>
                          <NavLink
                            className="flex-align-center"
                            to="/user/order"
                          >
                            <UnarchiveOutlinedIcon className="font-15 mr-6" />
                            <Typography className="font-12">
                              Đơn hàng của bạn
                            </Typography>
                          </NavLink>
                        </li>
                        <li>
                          <NavLink
                            className="flex-align-center"
                            to="/user/favorite"
                          >
                            <FavoriteBorderOutlinedIcon className="font-15 mr-6" />
                            <Typography className="font-12">
                              Sản phẩm yêu thích
                            </Typography>
                          </NavLink>
                        </li>
                        <li>
                          <NavLink
                            className="flex-align-center"
                            to="/user/comment"
                          >
                            <ChatOutlinedIcon className="font-15 mr-6" />
                            <Typography className="font-12">
                              Quản lí bình luận
                            </Typography>
                          </NavLink>
                        </li>
                        <li>
                          <NavLink
                            className="flex-align-center"
                            to="/user/rate"
                          >
                            <RateReviewOutlinedIcon className="font-15 mr-6" />
                            <Typography className="font-12">
                              Quản lí đánh giá
                            </Typography>
                          </NavLink>
                        </li>
                        <li>
                          <NavLink
                            className="flex-align-center"
                            to=""
                            onClick={() => handleLogout()}
                          >
                            <LogoutOutlinedIcon className="font-15 mr-6" />
                            <Typography className="font-12">
                              Đăng xuất
                            </Typography>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </span>
                ) : (
                  <Link to="/login">Đăng nhập</Link>
                )}
              </Grid>
            </Grid>
          </Container>
        </div>
        <div className={isInUserPage ? "d-none" : "flex-align-center"}>
          <Container
            maxWidth="lg"
            className={cx(styles.secondHeader, "flex-center  px-0")}
          >
            <Grid container className="flex-space-between">
              <Grid item className={cx(styles.Logo)} xs={3}>
                <Link to="/">
                  <img
                    src="https://hoanghamobile.com/Content/web/img/logo-text.png"
                    alt="logo"
                    width="90%"
                  />
                </Link>
              </Grid>
              <Grid item xs={6} style={{ position: "relative" }}>
                <form
                  onSubmit={handleSubmit((data: any) => {
                    console.log(data);
                  })}
                >
                  <input
                    className={cx(styles.searchBox, "font-14")}
                    placeholder="Hôm nay bạn cần tìm gì?"
                    // {...register("searchkey")}
                    onChange={e => handleSearch(e.target.value)}
                  />
                  <button className={cx(styles.searhBtn)} type="submit">
                    <SearchIcon />
                  </button>
                </form>
                {searchData.length > 0 && (
                  <div className={cx(styles.searchResult)}>
                    <ul>
                      {searchData.map((item: any) => (
                        <Link
                          to={"/product/:id".replace(":id", item?.id)}
                          onClick={() => {
                            setSearchData([]);
                          }}
                        >
                          <li>
                            <Grid container style={{ maxHeight: "70px" }}>
                              <Grid item xs={2}>
                                <img src={item.imgUrl} alt="tét" width="70%" />
                              </Grid>
                              <Grid
                                item
                                xs={10}
                                className="flex-direction-column flex-space-between d-flex"
                              >
                                <Grid item xs={12}>
                                  <Typography
                                    className="font-weight-bold"
                                    style={{ color: "#000" }}
                                  >
                                    {item.name}
                                  </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                  <Typography
                                    className="font-15 font-weight-bold"
                                    style={{ color: "#fd475a" }}
                                  >
                                    {formattedNumber(item.price)}
                                  </Typography>
                                </Grid>
                              </Grid>
                            </Grid>
                          </li>
                        </Link>
                      ))}
                    </ul>
                  </div>
                )}
              </Grid>

              <Grid
                item
                xs={3}
                className="d-flex flex-space-around"
                style={{ maxWidth: 245 }}
              >
                <Button
                  onClick={() => navigate("/order")}
                  className={cx(styles.shipBtn, "font-12")}
                  startIcon={
                    <LocalShippingOutlinedIcon
                      style={{ color: "#fff" }}
                      className="font-30"
                    />
                  }
                >
                  Kiểm tra đơn hàng
                </Button>

                <div>
                  <Link to="/cart">
                    <IconButton>
                      <Badge
                        badgeContent={totalCartItem()}
                        color="success"
                        className={cx(styles.badge)}
                      >
                        <ShoppingCartOutlinedIcon
                          className="font-40"
                          style={{ position: "relative" }}
                        />
                      </Badge>
                    </IconButton>
                  </Link>
                </div>
              </Grid>
            </Grid>
          </Container>
        </div>

        <div className={isInUserPage ? "d-none" : ""}>
          <Container
            style={{ display: "flex" }}
            maxWidth="lg"
            className={
              isSticky
                ? cx(styles.stickyNav)
                : cx(
                    styles.productNav,
                    "flex-align-center flex-justify-space-around"
                  )
            }
          >
            <Stack
              style={isSticky ? undefined : { height: 44 }}
              className="full-width"
              direction="row"
              justifyContent="space-around"
            >
              {PRODUCT_MENU_ITEM.map(item => (
                <div className={cx(styles.productNavItem)}>
                  <Link to="/laptop">
                    <div className="flex-align-center flex-direction-column">
                      <span>{item.icon}</span>
                      <span className="font-10">{item.title}</span>
                    </div>
                  </Link>
                  <div className={cx(styles.productSubNav)}>
                    <Grid container spacing={2} className="flex-space-around">
                      <Grid item xs={6} className="d-flex">
                        <Grid xs={8} className="pl-3">
                          <Typography className="font-weight-bold font-13">
                            HÃNG SẢN XUẤT
                          </Typography>
                          <div className="d-flex">
                            <Grid xs={4}>
                              <ul>
                                {BRAND_ITEM_1.map((items: string) => (
                                  <li>
                                    <Link
                                      to="/laptop"
                                      onClick={() =>
                                        sessionStorage.setItem("brand", items)
                                      }
                                    >
                                      {items}
                                    </Link>
                                  </li>
                                ))}
                              </ul>
                            </Grid>
                            <Grid xs={4}>
                              <ul>
                                {BRAND_ITEM_2.map(items => (
                                  <li>
                                    <Link
                                      to="/laptop"
                                      onClick={() =>
                                        sessionStorage.setItem("brand", items)
                                      }
                                    >
                                      {items}
                                    </Link>
                                  </li>
                                ))}
                              </ul>
                            </Grid>
                            <Grid xs={4}>
                              <ul>
                                {BRAND_ITEM_3.map(items => (
                                  <li>
                                    <Link
                                      to="/laptop"
                                      onClick={() =>
                                        sessionStorage.setItem("brand", items)
                                      }
                                    >
                                      {items}
                                    </Link>
                                  </li>
                                ))}
                              </ul>
                            </Grid>
                          </div>
                          <Grid xs={8}>
                            <Typography className="font-weight-bold font-13">
                              PHÂN LOẠI SẢN PHẨM
                            </Typography>
                            <ul>
                              <li>
                                <Link to="/laptop">Cao cấp - Sang trọng</Link>
                              </li>
                              <li>
                                <Link to="/laptop">Đồ họa kĩ thuật</Link>
                              </li>
                              <li>
                                <Link to="/laptop">Học tập văn phòng</Link>
                              </li>
                              <li>
                                <Link to="/laptop">Laptop Gaming</Link>
                              </li>
                              <li>
                                <Link to="/laptop">Mỏng nhẹ</Link>
                              </li>
                            </ul>
                          </Grid>
                        </Grid>
                        <Grid xs={4} className="pl-6">
                          <Typography className="font-weight-bold font-13">
                            MỨC GIÁ
                          </Typography>
                          <ul>
                            {PRICE_ITEM.map(items => (
                              <li>
                                <Link
                                  to="/laptop"
                                  onClick={() =>
                                    sessionStorage.setItem(
                                      "prices",
                                      JSON.stringify(items)
                                    )
                                  }
                                >
                                  {items.title}
                                </Link>
                              </li>
                            ))}
                          </ul>
                        </Grid>
                      </Grid>
                      <Grid item xs={6}>
                        <img
                          src="https://hoanghamobile.com/Uploads/2023/06/13/banner-xa-kho-laptop-01_638222682191504035.png"
                          alt="banner"
                        />
                      </Grid>
                    </Grid>
                  </div>
                </div>
              ))}
            </Stack>
          </Container>
        </div>
        <div className={isInUserPage ? "d-none" : cx(styles.chat)}>
          <button
            className={cx(styles.chatIcon, "flex-center")}
            onClick={() => setIsOpenChat(!isOpenChat)}
          >
            <QuestionAnswerIcon />
          </button>
          <div className={isOpenChat ? cx(styles.chatBox) : "d-none px-4"}>
            <Grid
              container
              className="d-flex p-2 flex-align-center"
              style={{ backgroundColor: "#009981" }}
            >
              <Grid item xs={8} className="d-flex flex-align-center">
                <div
                  className="mr-2"
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: "50%",
                    backgroundColor: "#fff",
                  }}
                >
                  <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/640px-User_icon_2.svg.png"
                    alt="admin"
                    width="100%"
                  />
                </div>
                <Typography>Admin</Typography>
              </Grid>
              <Grid item xs={2}>
                <div
                  className="flex-center"
                  style={{
                    width: 30,
                    height: 30,
                    borderRadius: " 50%",
                    backgroundColor: "#00bb9f",
                    color: "#fff",
                  }}
                >
                  <MoreHorizIcon />
                </div>
              </Grid>
              <Grid item xs={2}>
                <div
                  className="flex-center"
                  style={{
                    width: 30,
                    height: 30,
                    borderRadius: " 50%",
                    backgroundColor: "#00bb9f",
                    color: "#fff",
                  }}
                >
                  <KeyboardArrowDownIcon onClick={() => setIsOpenChat(false)} />
                </div>
              </Grid>
            </Grid>

            <div
              className="py-4"
              style={{ backgroundColor: "#fff", height: "100%" }}
            >
              <div className="d-flex">
                <div
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: "50%",
                    backgroundColor: "#fff",
                  }}
                >
                  <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/640px-User_icon_2.svg.png"
                    alt="admin"
                    width="100%"
                  />
                </div>
                <div className={cx(styles.chatText, "font-12")}>test chat</div>
              </div>

              <form
                onSubmit={handleSubmit((data: any) => {
                  console.log(data);
                })}
                style={{ width: "100%" }}
              >
                <Grid
                  container
                  className={cx(
                    styles.inputChat,
                    "font-12 d-flex flex-align-center"
                  )}
                >
                  <Grid item xs={10}>
                    <input
                      placeholder="Nhập tin nhắn, nhấn Enter để gửi"
                      {...register("chat")}
                    />
                  </Grid>
                  <Grid item xs={1}>
                    <AttachFileIcon className="font-22" />
                  </Grid>
                  <Grid item xs={1}>
                    <MoodIcon className="font-22" />
                  </Grid>
                </Grid>
              </form>
            </div>
          </div>
        </div>
        <div className="d-flex">
          <div className={isInUserPage ? cx(styles.sideBar) : "d-none"}>
            <div className="pl-5 py-8">
              <div>
                <Link to="/">
                  <img
                    src="https://hoanghamobile.com/Content/web/img/logo-text.png"
                    alt="logo"
                    width="90%"
                  />
                </Link>
              </div>

              <div className="d-flex flex-align-center mt-5 ">
                <Chip
                  className="font-40 font-weight-bold mt-1 mr-3"
                  label="U"
                  style={{
                    backgroundColor: "#009981",
                    color: "#fff",
                    borderRadius: 20,
                    height: 60,
                    width: 60,
                  }}
                />
                <span>
                  <Typography className="font-16 font-weight-bold">
                    User name
                  </Typography>
                  <div
                    className="d-flex flex-align-center"
                    style={{ color: "#999" }}
                  >
                    <PersonOutlineOutlinedIcon className="font-13 " />
                    <Typography className="font-13">
                      Thay đổi ảnh đại diện
                    </Typography>
                  </div>
                </span>
              </div>

              {/* side navigate */}
              <ul className="mt-9">
                <li
                  className={
                    location.pathname === "/user"
                      ? cx(styles.active, "mb-7")
                      : "mb-7"
                  }
                >
                  <NavLink className="flex-align-center" to="/user">
                    <TuneOutlinedIcon className="font-25 mr-6" />
                    <Typography className="font-weight-bold">
                      Bảng điều khiển
                    </Typography>
                  </NavLink>
                </li>
                <li
                  className={
                    location.pathname === "/user/info"
                      ? cx(styles.active, "mb-7")
                      : "mb-7"
                  }
                >
                  <NavLink className="flex-align-center" to="/user/info">
                    <AccountCircleOutlinedIcon className="font-25 mr-6" />
                    <Typography className="font-weight-bold">
                      Thông tin tài khoản
                    </Typography>
                  </NavLink>
                </li>
                <li
                  className={
                    location.pathname === "/user/order"
                      ? cx(styles.active, "mb-7")
                      : "mb-7"
                  }
                >
                  <NavLink className="flex-align-center" to="/user/order">
                    <UnarchiveOutlinedIcon className="font-25 mr-6" />
                    <Typography className="font-weight-bold">
                      Đơn hàng của bạn
                    </Typography>
                  </NavLink>
                </li>
                <li
                  className={
                    location.pathname === "/user/favorite"
                      ? cx(styles.active, "mb-7")
                      : "mb-7"
                  }
                >
                  <NavLink className="flex-align-center" to="/user/favorite">
                    <FavoriteBorderOutlinedIcon className="font-25 mr-6" />
                    <Typography className="font-weight-bold">
                      Sản phẩm yêu thích
                    </Typography>
                  </NavLink>
                </li>
                <li
                  className={
                    location.pathname === "/user/comment"
                      ? cx(styles.active, "mb-7")
                      : "mb-7"
                  }
                >
                  <NavLink className="flex-align-center" to="/user/comment">
                    <ChatOutlinedIcon className="font-25 mr-6" />
                    <Typography className="font-weight-bold">
                      Quản lí bình luận
                    </Typography>
                  </NavLink>
                </li>
                <li
                  className={
                    location.pathname === "/user/rate"
                      ? cx(styles.active, "mb-7")
                      : "mb-7"
                  }
                >
                  <NavLink className="flex-align-center" to="/user/rate">
                    <RateReviewOutlinedIcon className="font-25 mr-6" />
                    <Typography className="font-weight-bold">
                      Quản lí đánh giá
                    </Typography>
                  </NavLink>
                </li>
                <li className="mb-7">
                  <NavLink
                    className="flex-align-center"
                    to=""
                    onClick={() => store.dispatch(setIsNotLogedIn())}
                  >
                    <LogoutOutlinedIcon className="font-25 mr-6" />
                    <Typography className="font-weight-bold">
                      Đăng xuất
                    </Typography>
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>

          <div className={cx(styles.mainBody)}>
            <div className="mt-6">{children}</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default NavBar;
