import React from "react";

import SendOutlinedIcon from "@mui/icons-material/SendOutlined";
import { Button, Grid, TextField, Typography } from "@mui/material";
import cx from "classnames";

import styles from "./CommentSetion.module.scss";

const CommentSection = () => {
  return (
    <div className={cx(styles.root)}>
      <Grid container className="mt-5">
        <Grid item xs>
          {/* write comment section */}
          <div className={cx(styles.infoContainer, "py-4 px-8")}>
            <Typography className="font-20 font-weight-bold mt-5 mb-6">
              Bình luận về MacBook Pro 14 M2
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={4}>
                <TextField
                  variant="outlined"
                  placeholder="Họ tên *"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  variant="outlined"
                  placeholder="Điện thoại"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  variant="outlined"
                  placeholder="Email"
                  style={{ width: "100%" }}
                />
              </Grid>
            </Grid>
            <Grid container className="mt-3">
              <Grid item xs>
                <TextField
                  multiline
                  rows={4}
                  placeholder="Nội dung. Tối thiểu 15 kí tự"
                  style={{ width: "100%" }}
                />
              </Grid>
            </Grid>
            <Grid container className="mt-4">
              <Grid item xs={8}>
                <p className="font-12 " style={{ color: "#00483d" }}>
                  <i>
                    Để gửi bình luận, bạn cần nhập tối thiểu trường họ tên và
                    nội dung
                  </i>
                </p>
              </Grid>
              <Grid item xs={4}>
                <Button className={cx(styles.sendCmtBtn, "font-13")}>
                  <SendOutlinedIcon />
                  Gửi bình luận
                </Button>
              </Grid>
            </Grid>
            {/* show comment section */}
            <Grid container className="mt-8">
              <Grid item xs>
                <Grid container>
                  {/* user cmt */}
                  <Grid item xs={1}>
                    <div
                      style={{
                        borderRadius: "50%",
                        backgroundColor: "#5d5d5d",
                        width: 50,
                      }}
                    >
                      <img
                        src="https://w7.pngwing.com/pngs/741/68/png-transparent-user-computer-icons-user-miscellaneous-cdr-rectangle-thumbnail.png"
                        alt="avatar"
                        width={50}
                      />
                    </div>
                  </Grid>
                  <Grid item xs={8}>
                    <Typography className="font-weight-bold">User</Typography>
                    <p className="my-0 font-13" style={{ color: "#d4d4d4" }}>
                      <i>11/11/2023</i>
                    </p>
                    <Typography className="mb-6">
                      Sản phẩm có hỗ trợ trả góp không ạ ? Và hình thức trả góp
                      như thế nào ạ shop ?
                    </Typography>

                    {/* admin answer */}
                    <div
                      className="d-flex pl-5"
                      style={{ borderLeft: "2px solid #a7a9ac" }}
                    >
                      <Grid item xs={1}>
                        <div
                          style={{
                            borderRadius: "50%",
                            backgroundColor: "#5d5d5d",
                            width: 50,
                          }}
                        >
                          <img
                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/800px-User_icon_2.svg.png"
                            alt="avatar"
                            width={50}
                          />
                        </div>
                      </Grid>
                      <Grid item xs={8}>
                        <Typography className="font-weight-bold">
                          Admin
                        </Typography>
                        <p
                          className="my-0 font-13"
                          style={{ color: "#d4d4d4" }}
                        >
                          <i>11/11/2023</i>
                        </p>
                        <Typography>
                          Hiện bên em đang có 2 hình thức trả góp với sản phẩm
                          Apple tại trực tiếp chi nhánh: 💳Hình thức 1: Trả góp
                          thẻ tín dụng qua Payoo (từ 3 triệu) + Hỗ trợ tới 26
                          ngân hàng * Lãi suất: 0% trong 6 tháng * Phí chuyển
                          đổi 4% dựa trên số tiền trả góp - Với VPbank, SCB,
                          Sacombank, Shinhan, VIB phí chuyển đổi 5% - Ngân hàng
                          Techcombank mất thêm 1,1% phí chuyển đổi 📌Thủ tục:
                          Thẻ tín dụng + CCCD
                        </Typography>
                        <div>test</div>
                      </Grid>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default CommentSection;
