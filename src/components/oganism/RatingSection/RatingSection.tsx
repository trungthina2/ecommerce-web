import StarIcon from "@mui/icons-material/Star";
import { Grid, Rating, Stack, TextField, Typography } from "@mui/material";
import cx from "classnames";

import styles from "./RatingSection.module.scss";

const RatingSection = () => {
  return (
    <div>
      <Grid container className="mt-5">
        <Grid item xs>
          {/* write comment section */}
          <div className={cx(styles.infoContainer, "py-4 px-8")}>
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Typography className="font-20 font-weight-bold mt-5 mb-6">
                Đánh giá về MacBook Pro 14 M2
              </Typography>
              <div className="d-flex flex-center">
                <Rating
                  name="read-only"
                  value={1}
                  readOnly
                  emptyIcon={<StarIcon style={{ opacity: 0.55 }} />}
                />
                <span className="font-13">(TB/ 1 lượt đánh giá)</span>
              </div>
            </div>
            <Grid container spacing={2}>
              <Grid item xs={4}>
                <TextField
                  multiline
                  rows={4}
                  placeholder="Nội dung. Tối thiểu 15 kí tự"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4}>
                <div
                  style={{
                    height: "100%",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between",
                  }}
                >
                  <TextField
                    variant="outlined"
                    placeholder="Họ tên *"
                    style={{ width: "100%" }}
                  />
                  <TextField
                    variant="outlined"
                    placeholder="Số điện thoại mua hàngg"
                    style={{ width: "100%" }}
                  />
                </div>
              </Grid>
              <Grid item xs={4}>
                <div
                  style={{
                    height: "100%",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between",
                  }}
                >
                  <TextField
                    variant="outlined"
                    placeholder="Email"
                    style={{ width: "100%" }}
                  />
                  <Stack direction="row">
                    <Typography className="font-13 font-weight-bold">
                      Đánh giá của bạn:
                    </Typography>
                    <Rating
                      name="read-only"
                      value={0}
                      emptyIcon={<StarIcon style={{ opacity: 0.55 }} />}
                    />
                  </Stack>
                </div>
              </Grid>
            </Grid>
            {/* show rate section */}
            <Grid container className="mt-8">
              <Grid item xs>
                <Grid container>
                  {/* user rate */}
                  <Grid item xs={1}>
                    <div
                      style={{
                        borderRadius: "50%",
                        backgroundColor: "#5d5d5d",
                        width: 50,
                      }}
                    >
                      <img
                        src="https://w7.pngwing.com/pngs/741/68/png-transparent-user-computer-icons-user-miscellaneous-cdr-rectangle-thumbnail.png"
                        alt="avatar"
                        width={50}
                      />
                    </div>
                  </Grid>
                  <Grid item xs={8}>
                    <Typography className="font-weight-bold">User</Typography>
                    <Rating
                      name="read-only"
                      value={1}
                      readOnly
                      emptyIcon={<StarIcon style={{ opacity: 0.55 }} />}
                    />
                    <p className="my-0 font-13" style={{ color: "#d4d4d4" }}>
                      <i>11/11/2023</i>
                    </p>
                    <Typography className="mb-6">Sản phẩm tốt</Typography>

                    {/* admin answer */}
                    <div
                      className="d-flex pl-5"
                      style={{ borderLeft: "2px solid #a7a9ac" }}
                    >
                      <Grid item xs={1}>
                        <div
                          style={{
                            borderRadius: "50%",
                            backgroundColor: "#5d5d5d",
                            width: 50,
                          }}
                        >
                          <img
                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/800px-User_icon_2.svg.png"
                            alt="avatar"
                            width={50}
                          />
                        </div>
                      </Grid>
                      <Grid item xs={8}>
                        <Typography className="font-weight-bold">
                          Admin
                        </Typography>
                        <p
                          className="my-0 font-13"
                          style={{ color: "#d4d4d4" }}
                        >
                          <i>11/11/2023</i>
                        </p>
                        <Typography>Cảm ơn anh/ chị</Typography>
                      </Grid>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default RatingSection;
