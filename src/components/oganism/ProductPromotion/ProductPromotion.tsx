import { Typography } from "@mui/material";
import cx from "classnames";

import { EXTRA_SALE, SALE_TEXT } from "@app/constants/product-detail.constants";

import styles from "./ProductPromotion.module.scss";

const ProductPromotion = () => {
  return (
    <div
      className={cx(styles.root, "mt-5 py-3 px-2")}
      style={{ backgroundColor: "#FFF", borderRadius: 6 }}
    >
      <Typography className="font-14 font-weight-bold">
        ƯU ĐÃI THANH THOÁN
      </Typography>
      <ul className="px-3 font-13 my-0">
        {SALE_TEXT.map((item: string) => (
          <li>{item}</li>
        ))}
      </ul>
      <Typography className="font-14 font-weight-bold mt-3">
        ƯU ĐÃI ĐI KÈM
      </Typography>
      <ul className="px-3 font-13 my-0">
        {EXTRA_SALE.map((item: string) => (
          <li>{item}</li>
        ))}
      </ul>
    </div>
  );
};

export default ProductPromotion;
