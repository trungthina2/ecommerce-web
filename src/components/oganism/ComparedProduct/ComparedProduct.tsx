import { useState } from "react";

import SearchIcon from "@mui/icons-material/Search";
import TuneOutlinedIcon from "@mui/icons-material/TuneOutlined";
import { Button, Grid, Typography } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import cx from "classnames";
import { Link, useNavigate } from "react-router-dom";

import { FAKE_PRODUCT } from "@app/constants/product-detail.constants";
import { formattedNumber } from "@app/helpers/ultils";

import styles from "./ComparedProductc.module.scss";
import Sale from "../Sale/Sale";

type CompareItemProps = {
  data: any[];
};

const ComparedProduct = ({ data }: CompareItemProps) => {
  const navigate = useNavigate();
  const isLargeScreen = useMediaQuery("(min-width:1200px)");
  const [searchData, setSearchData] = useState<any[]>([]);

  const handleSearch = (value: string) => {
    if (value === "") {
      setSearchData([]);
      return;
    }
    setSearchData(
      FAKE_PRODUCT.filter((item: any) => {
        const lowerCaseItem = item.name.toLowerCase().trim();
        const lowerCaseSearchText = value.toLowerCase().trim();

        return lowerCaseItem.includes(lowerCaseSearchText);
      })
    );
  };

  return (
    <div className={cx(styles.root)}>
      <Grid container className="mt-5">
        <Grid item xs>
          <div
            className={cx(
              styles.infoContainer,
              "py-5 px-8 flex-align-center flex-direction-column"
            )}
          >
            <div
              className={cx(
                styles.compareTitle,
                `${
                  isLargeScreen ? "d-flex flex-space-between" : "text-center"
                } `
              )}
              style={{ width: "fit-content" }}
            >
              <Typography className="font-20 font-weight-bold">
                So sánh sản phẩm tương tự
              </Typography>
              <div className={cx(styles.searchBox)}>
                <input
                  onChange={e => handleSearch(e.target.value)}
                  placeholder="Nhập tên sản phẩm cần so sánh"
                />
                <button className={cx(styles.searhBtn)}>
                  <SearchIcon />
                </button>
                {searchData.length > 0 && (
                  <div className={cx(styles.searchResult)}>
                    <ul>
                      {searchData.map((item: any) => (
                        <Link
                          to="/compare/"
                          onClick={() => {
                            setSearchData([]);
                            sessionStorage.setItem(
                              "item2Id",
                              JSON.stringify(item.id)
                            );
                            sessionStorage.setItem("isDeleteItem2Id", "false");
                          }}
                        >
                          <li>
                            <Grid container style={{ maxHeight: "70px" }}>
                              <Grid item xs={2}>
                                <img src={item.imgUrl} alt="tét" width="70%" />
                              </Grid>
                              <Grid
                                item
                                xs={10}
                                className="flex-direction-column flex-space-between d-flex"
                              >
                                <Grid item xs={12}>
                                  <Typography
                                    className="font-weight-bold"
                                    style={{ color: "#000" }}
                                  >
                                    {item.name}
                                  </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                  <Typography
                                    className="font-15 font-weight-bold"
                                    style={{ color: "#fd475a" }}
                                  >
                                    {formattedNumber(item.price)}
                                  </Typography>
                                </Grid>
                              </Grid>
                            </Grid>
                          </li>
                        </Link>
                      ))}
                    </ul>
                  </div>
                )}
              </div>
            </div>

            <Grid
              container
              columns={10}
              spacing={2}
              className="flex-space-between mt-7"
            >
              <div className={cx(styles.compareItems, "d-flex")}>
                {data.map((item: any) => (
                  <Grid
                    item
                    xs={2}
                    className={cx(
                      styles.compareItem,
                      "flex-direction-column flex-align-center"
                    )}
                  >
                    <img src={item?.image} alt="product" />
                    <div
                      className={cx(
                        styles.sticker,
                        "flex-direction-column flex-justify-start"
                      )}
                    >
                      <img
                        src="https://hoanghamobile.com/Content/web/sticker/apple.png"
                        alt="sticker"
                      />
                      <img
                        src="https://hoanghamobile.com/Content/web/sticker/bao-hanh-12t.png"
                        alt="sticker"
                        width={46}
                      />
                    </div>
                    <Typography className="font-13 font-weight-bold text-center">
                      {item?.name}
                    </Typography>
                    <Typography
                      className="font-weight-bold"
                      style={{ color: "#fd475a" }}
                    >
                      {item?.price}
                    </Typography>
                    <Button
                      className={cx(styles.compareBtn, "mt-3")}
                      startIcon={<TuneOutlinedIcon />}
                      onClick={() => {
                        sessionStorage.setItem(
                          "item2Id",
                          JSON.stringify(item.id)
                        );
                        sessionStorage.setItem("isDeleteItem2Id", "false");
                        navigate("/compare/");
                      }}
                    >
                      So sánh
                    </Button>
                    <div className={cx(styles.saleBox)}>
                      <Sale />
                    </div>
                  </Grid>
                ))}
              </div>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default ComparedProduct;
