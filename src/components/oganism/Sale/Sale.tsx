import React from "react";

import { Chip, Typography } from "@mui/material";
import cx from "classnames";

import styles from "./Sale.module.scss";

const Sale = () => {
  return (
    <div className={cx(styles.root)}>
      <Typography
        className="font-14 font-weight-bold"
        style={{ color: "#009a82" }}
      >
        KHUYẾN MÃI
      </Typography>
      <div>
        <Chip label="KM1" />
        <p className="font-13 mt-0 mb-1">
          GIẢM THÊM 200.000đ khi thanh toán qua VNPAY-QR.
        </p>
      </div>
      <div>
        <Chip label="KM2" />
        <p className="font-13 mt-0 mb-1">
          Ưu đãi mua Combo 4 món Củ sạc innostyle 30w + Dán MH, Camera + Ốp lưng
          iP14
        </p>
      </div>
      <div>
        <Chip label="KM3" />
        <p className="font-13 mt-0 mb-1">
          Ưu đãi mua Combo 5 món Củ sạc Mophie 30w + Dán MH, Camera + Ốp lưng,
          Sạc Dự phòng iPhone 14 Series
        </p>
      </div>
    </div>
  );
};

export default Sale;
