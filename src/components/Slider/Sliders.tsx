import React from "react";

import cx from "classnames";
import Slider from "react-slick";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";

import styles from "./Slider.module.scss";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const LIST_BANNER = [
  {
    id: 1,
    image: "14prm-pc.png",
    title: "iPhone 14 Pro Max VN/A",
    contents: "Giá chỉ từ 25.390.000đ",
  },
  {
    id: 2,
    image: "man-hinh-viewsonic_638211399154458704.jpeg",
    title: "Tuần lễ Laptop văn phòng",
    contents: "Làm việc năng suất, giá rẻ phát ngất",
  },
  {
    id: 3,
    image: "tuan-le-laptop-gaming-02.jpeg",
    title: "Samsung Galaxy S20 FE",
    contents: "Ưu đãi lên tới 60%",
  },
  {
    id: 4,
    image: "tv-xiaomi-01.jpeg",
    title: "Smart TV 4K giá shock",
    contents: "Chỉ từ 6.990.000đ",
  },
];
const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
};
class SliderComponent extends React.Component {
  constructor(props: any) {
    super(props);
    this.state = {
      index: 0,
    };
  }

  handleChange = (value: any) => {
    this.setState({
      index: value,
    });
  };

  handleChangeIndex = (index: any) => {
    this.setState({
      index,
    });
  };

  render() {
    const { index } = this.state;

    return (
      <div>
        <AutoPlaySwipeableViews
          index={index}
          onChangeIndex={this.handleChangeIndex}
          enableMouseEvents
        >
          {LIST_BANNER.map(item => {
            return (
              <div className={cx(styles.slide)} key={item.id}>
                <img
                  className={cx(styles.img_banner)}
                  src={`/img/${item.image}`}
                  alt=""
                />
              </div>
            );
          })}
        </AutoPlaySwipeableViews>
        <Slider {...settings}>
          {LIST_BANNER.map((item, i) => {
            return (
              <div className={cx(styles.button_tab_container)}>
                <button
                  onClick={() => this.handleChange(i)}
                  className={cx(
                    styles.button_tab,
                    index === i ? styles.active : ""
                  )}
                >
                  <p>{item.title}</p>
                  <small>{item.contents}</small>
                </button>
              </div>
            );
          })}
        </Slider>
      </div>
    );
  }
}

export default SliderComponent;
